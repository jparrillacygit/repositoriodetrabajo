// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
/*
OLD:
apiKey: "AIzaSyCvUcf-UT8vRKgxikZEE83xZ6femaiYE2k",
    authDomain: "aitana-eb442.firebaseapp.com",
    databaseURL: "https://aitana-eb442-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "aitana-eb442",
    storageBucket: "aitana-eb442.appspot.com",
    messagingSenderId: "944272723350",
    appId: "1:944272723350:web:af22edb7b5bf4dc53e61b7",
    measurementId: "G-69E77MWPF6"

*/
export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyDxpa_F0pNQu_U1_FnO2fGV4CYmZAgYfR8",
    authDomain: "aitana-angular.firebaseapp.com",
    databaseURL: "https://aitana-angular.firebaseio.com",
    projectId: "aitana-angular",
    storageBucket: "aitana-angular.appspot.com",
    messagingSenderId: "436354205196",
    appId: "1:436354205196:web:da1eb1c1aadaad798e1029",
    measurementId: "G-V747HPJFBM"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
