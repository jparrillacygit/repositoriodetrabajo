import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'aitana001a',
    pathMatch: 'full'
  },
  {
    path: 'aitana001a',
    loadChildren: () => import('./paginas/paginasDeLogin/paginaLogin/aitana001a.module').then( m => m.Aitana001aPageModule)
  },
  {
    path: 'aitana001b',
    loadChildren: () => import('./paginas/paginasDeLogin/recuperarPasss/aitana001b.module').then( m => m.Aitana001bPageModule)
  },
  {
    path: 'aitana001e',
    loadChildren: () => import('./paginas/paginasDeLogin/paginaIniciarConCodigoSeguridad/aitana001e.module').then( m => m.Aitana001ePageModule)
  },
  {
    path: 'aitana002a',
    loadChildren: () => import('./paginas/paginasHuellas/pHuella/aitana002a.module').then( m => m.Aitana002aPageModule)

  },
  {
    path: 'aitana002b',
    loadChildren: () => import('./paginas/paginasHuellas/pHuellaConPass/aitana002b.module').then( m => m.Aitana002bPageModule)
  },
  {
    path: 'aitana004d',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pCambiarPass/aitana004d.module').then( m => m.Aitana004dPageModule)
  },
  {
    path: 'aitana001c',
    children:[
      {
        path:"",
        loadChildren: () => import('./paginas/paginasDeLogin/pagRecuperarPassAddPass/aitana001c.module').then( m => m.Aitana001cPageModule)
      },
      {
        path:":correo",//cuando escribas un numero carga la siguiente ventana
        loadChildren: () => import('./paginas/paginasDeLogin/pagRecuperarPassAddPass/aitana001c.module').then( m => m.Aitana001cPageModule)
      }
    ]
    

  },
  {
    path: 'aitana001d',
    loadChildren: () => import('./paginas/paginasDeLogin/paginaRegistrar/aitana001d.module').then( m => m.Aitana001dPageModule)
  },
  
  {
    path: 'aitana003b',
    loadChildren: () => import('./paginas/paginasVerifficarCreacionUsuario/pReenvConfirmacion/aitana003b.module').then( m => m.Aitana003bPageModule)
  },
  {
    path: 'aitana003a',
    loadChildren: () => import('./paginas/paginasVerifficarCreacionUsuario/pConfEnvioCorreo/aitana003a.module').then( m => m.Aitana003aPageModule)
  },
  {
    path: 'aitana004a',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pPerfilUsuario/aitana004a.module').then( m => m.Aitana004aPageModule)
  },
  {
    path: 'aitana004e',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pMensajePendientes/aitana004e.module').then( m => m.Aitana004ePageModule)
  },
  {
    path: 'aitana004j',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pReportarError/aitana004j.module').then( m => m.Aitana004jPageModule)
  },
  {
    path: 'aitana004c',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pConfHuellaYPass/aitana004c.module').then( m => m.Aitana004cPageModule)
  },
  {
    path: 'aitana004b',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pCambiarDatosUsuario/aitana004b.module').then( m => m.Aitana004bPageModule)
  },
  {
    path: 'aitana004f',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pDocumentacionPendiente/aitana004f.module').then( m => m.Aitana004fPageModule)
  },
  
  {
    path: 'aitana004h',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pMiFacturacion/aitana004h.module').then( m => m.Aitana004hPageModule)
  },
  {
    path: 'aitana004g',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pMiFacturacionAutorizado/aitana004g.module').then( m => m.Aitana004gPageModule)
  },
  {
    path: 'aitana004i',
    loadChildren: () => import('./paginas/paginasAjustesUsuario/pMiFacturacionPedidos/aitana004i.module').then( m => m.Aitana004iPageModule)
  },
  {
    path: 'aitana00b',
    loadChildren: () => import('./paginas/aitana00b/aitana00b.module').then( m => m.Aitana00bPageModule)
  },
  {
    path: 'aitana005a',
    loadChildren: () => import('./paginas/paginasTickets/pTicketsHoy/aitana005a.module').then( m => m.Aitana005aPageModule)
  },
  {
    path: 'aitana005c',
   // loadChildren: () => import('./paginas/aitana005/aitana005c/aitana005c.module').then( m => m.Aitana005cPageModule)
   children:[
    {
      path:"",
      loadChildren: () => import('./paginas/paginasTickets/pTicketsTodos/aitana005c.module').then( m => m.Aitana005cPageModule)
    },
    {
      path:":id",
      loadChildren: () => import('./paginas/paginasTickets/pTicketsTodos/aitana005c.module').then( m => m.Aitana005cPageModule)
    }
  ]
  },
  {
    path: 'aitana005d',
    loadChildren: () => import('./paginas/paginasTickets/pTicketsDiaSiguiente/aitana005d.module').then( m => m.Aitana005dPageModule)
  },
  {
    path: 'aitana006a',
    //loadChildren: () => import('./paginas/aitanas006/aitana006a/aitana006a.module').then( m => m.Aitana006aPageModule)
    children:[
      {
        path:"",
        loadChildren: () => import('./paginas/paginasInfoTicket/pDatosTicket/aitana006a.module').then( m => m.Aitana006aPageModule)
      },
      {
        path:":id",
        loadChildren: () => import('./paginas/paginasInfoTicket/pDatosTicket/aitana006a.module').then( m => m.Aitana006aPageModule)
      }
    ]
  },
  {
    path: 'aitana006d',
    children:[
      {
        path:"",
        loadChildren: () => import('./paginas/paginasInfoTicket/pDireccionTrabajo/aitana006d.module').then( m => m.Aitana006dPageModule)
      },
      {
        path:":latitud/:longitud/:idTicket",
        loadChildren: () => import('./paginas/paginasInfoTicket/pDireccionTrabajo/aitana006d.module').then( m => m.Aitana006dPageModule)
      }
    ]
  },
  {
    path: 'aitana006e',
   // loadChildren: () => import('./paginas/aitanas006/aitana006e/aitana006e.module').then( m => m.Aitana006ePageModule)
    children:[
      {
        path:"",
        loadChildren: () => import('./paginas/paginasInfoTicket/pCitasTicket/aitana006e.module').then( m => m.Aitana006ePageModule)
      },
      {
        path:":id",
        loadChildren: () => import('./paginas/paginasInfoTicket/pCitasTicket/aitana006e.module').then( m => m.Aitana006ePageModule)
      }
    ]
  },
  {
    path: 'aitana006f',
    // loadChildren: () => import('./paginas/aitanas006/aitana006f/aitana006f.module').then( m => m.Aitana006fPageModule)
    children:[
      {
        path:"",
        loadChildren: () => import('./paginas/paginasInfoTicket/pCitasTicketAceptarONo/aitana006f.module').then( m => m.Aitana006fPageModule)
      },
      {
        path:":id/:citaId",
        loadChildren: () => import('./paginas/paginasInfoTicket/pCitasTicketAceptarONo/aitana006f.module').then( m => m.Aitana006fPageModule)
      }
    ]
  },
  {
    path: 'aitana006g',
    loadChildren: () => import('./paginas/paginasInfoTicket/pCitasTicketFecha/aitana006g.module').then( m => m.Aitana006gPageModule)
  },
  {
    path: 'aitana006h',
    loadChildren: () => import('./paginas/paginasInfoTicket/pSeguimientisEnviar/aitana006h.module').then( m => m.Aitana006hPageModule)
  },
  {
    path: 'aitana006j',
    children:[
      {
        path:"",
        loadChildren: () => import('./paginas/paginasInfoTicket/pSeguimientosVer/aitana006j.module').then( m => m.Aitana006jPageModule)
      },
      {
        path:":id",
        loadChildren: () => import('./paginas/paginasInfoTicket/pSeguimientosVer/aitana006j.module').then( m => m.Aitana006jPageModule)
      }
    ]
  },
  {
    path: 'aitana006l',
   // loadChildren: () => import('./paginas/aitanas006/aitana006l/aitana006l.module').then( m => m.Aitana006lPageModule)
   children:[
    {
      path:"",
      loadChildren: () => import('./paginas/paginasInfoTicket/pPiezas/aitana006l.module').then( m => m.Aitana006lPageModule)
    },
    {
      path:":id",
      loadChildren: () => import('./paginas/paginasInfoTicket/pPiezas/aitana006l.module').then( m => m.Aitana006lPageModule)
    }
  ]
  },
  {
    path: 'aitana006k',
    loadChildren: () => import('./paginas/paginasInfoTicket/pSeguimientosFiltrarVision/aitana006k.module').then( m => m.Aitana006kPageModule)
  },
  {
    path: 'aitana006o',
    loadChildren: () => import('./paginas/paginasInfoTicket/pDiagnosisAdd/aitana006o.module').then( m => m.Aitana006oPageModule)
  },
  {
    path: 'aitana006n',
    loadChildren: () => import('./paginas/paginasInfoTicket/pReparacion/aitana006n.module').then( m => m.Aitana006nPageModule)
  },
  {
    path: 'aitana006m',
   // loadChildren: () => import('./paginas/aitanas006/aitana006m/aitana006m.module').then( m => m.Aitana006mPageModule)
   children:[
    {
      path:"",
      loadChildren: () => import('./paginas/paginasInfoTicket/pDiagnosisEditar/aitana006m.module').then( m => m.Aitana006mPageModule)
    },
    {
      path:":id/:ticketID",
      loadChildren: () => import('./paginas/paginasInfoTicket/pDiagnosisEditar/aitana006m.module').then( m => m.Aitana006mPageModule)
    }
  ]
  },
  {
    path: 'aitana006p',
    loadChildren: () => import('./paginas/paginasInfoTicket/pDocumentos/aitana006p.module').then( m => m.Aitana006pPageModule)
  },
  {
    path: 'aitana006q',
    loadChildren: () => import('./paginas/paginasInfoTicket/pDocumentosAdd/aitana006q.module').then( m => m.Aitana006qPageModule)
  },
  {
    path: 'aitana006r',
    loadChildren: () => import('./paginas/paginasInfoTicket/pCentroConocimiento/aitana006r.module').then( m => m.Aitana006rPageModule)
  },
  {
    path: 'aitana006s',
    loadChildren: () => import('./paginas/paginasInfoTicket/pCentroConociminetoFiltrar/aitana006s.module').then( m => m.Aitana006sPageModule)
  },
  {
    path: 'filtrado-ticket',
    
    children:[
      {
        path:"",
        loadChildren: () => import('./paginas/paginasTickets/filtrado-ticket/filtrado-ticket.module').then( m => m.FiltradoTicketPageModule)
      },
      {
        path:":ruta",
        loadChildren: () => import('./paginas/paginasTickets/filtrado-ticket/filtrado-ticket.module').then( m => m.FiltradoTicketPageModule)
      }
    ]
  },
  
  
  {
    path: 'getdireccion',
    children:[
      {
        path:"",
        loadChildren: () => import('./paginas/paginasInfoTicket/ejemplo/ejemplo.module').then( m => m.EjemploPageModule)
      },
      {
        path:":idTicket",
        loadChildren: () => import('./paginas/paginasInfoTicket/ejemplo/ejemplo.module').then( m => m.EjemploPageModule)
      }
    ]
  },

  





];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }

