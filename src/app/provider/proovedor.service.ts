import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProovedorService {

  constructor(private httpClient: HttpClient) { }
}
