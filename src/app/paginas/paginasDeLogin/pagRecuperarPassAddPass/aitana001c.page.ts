import { Component, OnInit } from '@angular/core';
import { Router ,ActivatedRoute} from '@angular/router';
import { IonInput, ToastController } from '@ionic/angular';
import { RecuperarPasswordService } from '../../../servicios/usuario/recover-password/recuperar-password.service';

@Component({
  selector: 'app-aitana001c',
  templateUrl: './aitana001c.page.html',
  styleUrls: ['./aitana001c.page.scss','../../../estiloComun/estilosComun001.scss'],
})
export class Aitana001cPage implements OnInit {
  private mensajeError:string=""
  correo="";
  showPassword = false;
  iconoPassword = 'eye';

  constructor(private router:Router,private recuperar:RecuperarPasswordService,private activateRoute:ActivatedRoute,
    private toastController:ToastController) { }

  ngOnInit() {
  }
  obtenerCorreo(){
    this.activateRoute.paramMap.subscribe(
      paramMap =>{
        this.correo= paramMap.get('correo')//es el path que le habiamos asignado
        
        
      }
    )
  }
  cambiarPass(codigo:IonInput,nuevaPass:IonInput,reNuevaPass:IonInput ){
    this.obtenerCorreo();
    console.log("Correo: "+this.correo+" codigo: "+codigo.value+" passS "+nuevaPass.value+" "+reNuevaPass.value)
    if(codigo.value =="" || nuevaPass.value=="" || reNuevaPass.value=="") {
      this.mensajeError="Debes introducir los datos requeridos en todos los campos"
      this.lanzarMensajeError();
    }else{
      if(nuevaPass.value==reNuevaPass.value){
        console.log("Correo: "+this.correo+" codigo: "+codigo.value+" correo: "+this.correo)
        this.recuperar.cambiarPass(codigo.value+"",nuevaPass.value+"",this.correo).subscribe(
          data => {
            this.router.navigate(['/aitana005a']);
            console.log("Inicio");
          },
          error => {
            this.mensajeError=error.error.message+".";
            this.lanzarMensajeError();
          }
        )
      }else{
        this.mensajeError="La dos contraseñas deben ser iguales."
        this.lanzarMensajeError();
      }
      
    }
    
    
  }
  mostrarPassword(): void {
    this.showPassword = !this.showPassword;

    if(this.iconoPassword == 'eye'){
      
      this.iconoPassword = 'eye-off';

    }else{
      this.iconoPassword = 'eye';
    }
    
  }
  async lanzarMensajeError(){
    const toast = await this.toastController.create({
      duration:4000,
      cssClass:"error",
      message: this.mensajeError,
      position: 'bottom',

      
    });
    await toast.present();
  }
}
