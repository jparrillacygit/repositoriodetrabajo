import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana001cPage } from './aitana001c.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana001cPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana001cPageRoutingModule {}
