import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana001cPageRoutingModule } from './aitana001c-routing.module';

import { Aitana001cPage } from './aitana001c.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana001cPageRoutingModule,
    
  ],
  declarations: [Aitana001cPage]
})
export class Aitana001cPageModule {}
