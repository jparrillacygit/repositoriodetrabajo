import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana001dPageRoutingModule } from './aitana001d-routing.module';

import { Aitana001dPage } from './aitana001d.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana001dPageRoutingModule
  ],
  declarations: [Aitana001dPage]
})
export class Aitana001dPageModule {}
