import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana001dPage } from './aitana001d.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana001dPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana001dPageRoutingModule {}
