import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-aitana001d',
  templateUrl: './aitana001d.page.html',
  styleUrls: ['./aitana001d.page.scss','../../../estiloComun/estilosComun001.scss'],
})
export class Aitana001dPage implements OnInit {
  private mensajeError:string="";
  showPassword = false;
  iconoPassword = 'eye';

  constructor(private toastController:ToastController) { }

  ngOnInit() {
  }


  mostrarPassword(): void {
    this.showPassword = !this.showPassword;

    if(this.iconoPassword == 'eye'){
      
      this.iconoPassword = 'eye-off';

    }else{
      this.iconoPassword = 'eye';
    }
    
  }
  registrar(nombre:HTMLInputElement,apellido:HTMLInputElement,correo:HTMLInputElement,pass:HTMLInputElement,passRe:HTMLInputElement){
      if(nombre.value=="" ||apellido.value=="" || correo.value=="" || pass.value=="" || passRe.value==""){
        this.mensajeError="Debes rellenar todos los campos.";
        this.lanzarMensajeError();
      }else{
          if(pass.value==passRe.value){
            this.mensajeError="";
          }else{
            this.mensajeError="La contraseña debe ser igual";
            this.lanzarMensajeError();
          }
      }
  }
  async lanzarMensajeError(){
    const toast = await this.toastController.create({
      duration:4000,
      cssClass:"error",
      message: this.mensajeError,
      position: 'bottom',

      
    });
    await toast.present();
  }
}
