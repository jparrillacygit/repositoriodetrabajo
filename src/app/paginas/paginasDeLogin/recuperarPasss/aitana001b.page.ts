import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonInput, ToastController } from '@ionic/angular';
import { RecuperarPasswordService } from '../../../servicios/usuario/recover-password/recuperar-password.service';

@Component({
  selector: 'app-aitana001b',
  templateUrl: './aitana001b.page.html',
  styleUrls: ['./aitana001b.page.scss',
              '../../../estiloComun/estilosComun001.scss'
            ],
})
export class Aitana001bPage implements OnInit {
  private mensajeError:string="";
  constructor(private router:Router,private recuperar:RecuperarPasswordService,
    private toastController:ToastController) { }

  ngOnInit() {
  }
  comprobarCorreo(correo:IonInput){
    if(correo.value!=""){
      this.recuperar.pedirCorreo(correo.value+"").subscribe(
        data => {
          this.router.navigate(['/aitana001c',correo.value+""]);
          console.log("Inicio");
        },
        error => {
          this.mensajeError=error.error.message+".";
          this.lanzarMensajeError();
        }
      )
    }else{
      this.mensajeError="Debes introducir el correo.";
      this.lanzarMensajeError();
    }
    
    
  }
  async lanzarMensajeError(){
    const toast = await this.toastController.create({
      duration:4000,
      cssClass:"error",
      message: this.mensajeError,
      position: 'bottom',

      
    });
    await toast.present();
  }
}
