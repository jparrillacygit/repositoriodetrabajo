import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana001bPage } from './aitana001b.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana001bPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana001bPageRoutingModule {}
