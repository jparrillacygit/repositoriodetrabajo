import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana001bPageRoutingModule } from './aitana001b-routing.module';

import { Aitana001bPage } from './aitana001b.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana001bPageRoutingModule,
    
  ],
  declarations: [Aitana001bPage]
})
export class Aitana001bPageModule {}
