import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {Router} from '@angular/router'
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { IonIcon, IonInput, MenuController, ModalController, NavController, ToastController,NavParams } from '@ionic/angular';
import { FirebaseAuth } from 'angularfire2';
import { IniciarSesionService } from '../../../servicios/usuario/login-service/iniciar-sesion.service';
import firebase from 'firebase/app';
import 'firebase/auth'
@Component({
  selector: 'app-aitana001a',
  templateUrl: './aitana001a.page.html',
  styleUrls: ['../../../estiloComun/estilosComun001.scss',
    './aitana001a.page.scss'
              
  ],
})
export class Aitana001aPage implements OnInit {
  private contVisible:boolean=true;
  private mensajeError:string='';
  constructor(private router:Router,public menuCtr:MenuController ,private login:IniciarSesionService,
    private modalController: ModalController,
    private toastController:ToastController,
    private googlePlus:GooglePlus,
    public navCtrl:NavController,
    private afAuth:AngularFireAuth
    ){ 
    
    

  }
  
  ngOnInit() {
    
    this.menuCtr.enable(false);
    
    
  }
  verPass(pass:HTMLInputElement,icono:IonIcon){
    
    if(this.contVisible===true){
      this.contVisible=false;
      
      pass.type="text";
      icono.name="eye-off";
    }else{
      this.contVisible=true;
      pass.type="password";
      icono.name="eye";
    }
  }
  //aitana001e
  // Dismiss Login Modal
  dismissLogin() {
    this.modalController.dismiss();
  }
  // On Register button tap, dismiss login modal and open register modal
  
  iniciarSesion(usuario:IonInput,pass:IonInput){
    
    
    console.log("Devuelve:"+usuario.value)
    if( usuario.value!="" || pass.value!=""){
      this.login.login(usuario.value+"",pass.value+"").subscribe(
        data => {
          console.log("en el data")
  
          this.router.navigate(['/aitana001e']);
          //this.router.navigate(['/aitana005a']);

        },
        error => {
          this.mensajeError=error.error.message+".";
          this.lanzarMensajeError();
          
        }
      )
    }else{

      this.mensajeError="Debes introducir datos en los dos campos."
      this.lanzarMensajeError();
    }
    
    

  }
  async lanzarMensajeError(){
    const toast = await this.toastController.create({
      duration:4000,
      cssClass:"error",
      message: this.mensajeError,
      position: 'bottom',

      
    });
    await toast.present();
  }
  
  async iniciarConGoogle() {
    
       //await this.login.loginConGoogle();
        await this.googlePlus.login({}).then(result => {
          this.mensajeError="email: "+result.email+ " token: "+result.idToken;
          
          this.login.loginConEmailYToken(result.email+"",result.idToken+"").subscribe(
            data => {
      
              this.router.navigate(['/aitana005a']);
            },
            error => {
              //console.log("ERROR");
              this.mensajeError=error.error.message+".";
              this.lanzarMensajeError();
            }
          )
        })
        .catch(err =>  {
          console.log("ERROR: ");
          console.log(err);
          this.mensajeError=err;
          this.lanzarMensajeError();
        });;
    
      /*
      await this.afAuth.authState.subscribe(
        res=>{
          let usuario = JSON.parse(JSON.stringify(res));
          this.mensajeError="EMAIL: "+usuario.email+" TOKEN: "+usuario.stsTokenManager.accessToken;
          
          this.login.loginConEmailYToken(usuario.email+"",usuario.stsTokenManager.accessToken).subscribe(
            data => {
      
              this.router.navigate(['/aitana005a']);
            },
            error => {
              //console.log("ERROR");
              this.mensajeError=error.error.message+".";
              this.lanzarMensajeError();
            }
          )
        }
      ) */
      
   
    
  }
   
}
