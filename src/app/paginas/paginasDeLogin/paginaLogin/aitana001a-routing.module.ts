import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana001aPage } from './aitana001a.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana001aPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana001aPageRoutingModule {}
