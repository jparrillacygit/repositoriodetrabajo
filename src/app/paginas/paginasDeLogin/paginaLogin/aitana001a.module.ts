import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana001aPageRoutingModule } from './aitana001a-routing.module';

import { Aitana001aPage } from './aitana001a.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana001aPageRoutingModule,
    
  ],
  declarations: [Aitana001aPage]
})
export class Aitana001aPageModule {}
