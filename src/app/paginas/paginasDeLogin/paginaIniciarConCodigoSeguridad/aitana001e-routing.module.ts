import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana001ePage } from './aitana001e.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana001ePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana001ePageRoutingModule {}
