import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import {ComprobarCodigoSeguridadService} from '../../../servicios/usuario/comprobe-securityCode/comprobar-codigo-seguridad.service';

@Component({
  selector: 'app-aitana001e',
  templateUrl: './aitana001e.page.html',
  styleUrls: ['../../../estiloComun/estilosComun001.scss','./aitana001e.page.scss'],
})
export class Aitana001ePage implements OnInit {
  private mensajeError:string="";
  constructor(private router:Router, private comprobarCodigoService : ComprobarCodigoSeguridadService,
    private toastController:ToastController) { }

  ngOnInit() {
  }
  validarCodigo(codigo:HTMLInputElement){
    if(codigo.value!=""){
      this.comprobarCodigoService.comprobarCodigo(codigo.value).subscribe(
        (data) =>{
          this.router.navigate(['/aitana005a'])
        },
        (error) =>{
          this.mensajeError=error.error.message+".";
            console.log(error);
            this.lanzarMensajeError();
        }
      );
    }else{
      this.mensajeError="Debes introducir el codigo de seguridad."
      this.lanzarMensajeError();
    }
    
  }
  async lanzarMensajeError(){
    const toast = await this.toastController.create({
      duration:4000,
      cssClass:"error",
      message: this.mensajeError,
      position: 'bottom',

      
    });
    await toast.present();
  }
}
