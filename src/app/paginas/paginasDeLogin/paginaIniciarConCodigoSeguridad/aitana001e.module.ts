import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana001ePageRoutingModule } from './aitana001e-routing.module';

import { Aitana001ePage } from './aitana001e.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana001ePageRoutingModule
  ],
  declarations: [Aitana001ePage]
})
export class Aitana001ePageModule {}
