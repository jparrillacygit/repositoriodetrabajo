import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004iPage } from './aitana004i.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004iPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004iPageRoutingModule {}
