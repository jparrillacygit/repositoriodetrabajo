import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-aitana004i',
  templateUrl: './aitana004i.page.html',
  styleUrls: ['./aitana004i.page.scss'],
})
export class Aitana004iPage implements OnInit {

  constructor( private menuCtrl:MenuController ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }

}
