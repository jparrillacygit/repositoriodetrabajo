import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004iPageRoutingModule } from './aitana004i-routing.module';

import { Aitana004iPage } from './aitana004i.page';
import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004iPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004iPage]
})
export class Aitana004iPageModule {}
