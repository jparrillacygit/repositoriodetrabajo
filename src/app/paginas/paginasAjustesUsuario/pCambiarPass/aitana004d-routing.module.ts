import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004dPage } from './aitana004d.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004dPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004dPageRoutingModule {}
