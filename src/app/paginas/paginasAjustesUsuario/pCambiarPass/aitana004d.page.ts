import { Component, OnInit } from '@angular/core';
import {MenuController} from '@ionic/angular'

@Component({
  selector: 'app-aitana004d',
  templateUrl: './aitana004d.page.html',
  styleUrls: ['./aitana004d.page.scss'],
})
export class Aitana004dPage implements OnInit {


  showPassword = false;
  iconoPassword = 'eye';

  constructor(private menuControler : MenuController) { }

  

  ngOnInit() {
    this.menuControler.enable(true);
  }

  mostrarPassword(): void {
    this.showPassword = !this.showPassword;

    if(this.iconoPassword == 'eye'){
      
      this.iconoPassword = 'eye-off';

    }else{
      this.iconoPassword = 'eye';
    }
    
  }

}
