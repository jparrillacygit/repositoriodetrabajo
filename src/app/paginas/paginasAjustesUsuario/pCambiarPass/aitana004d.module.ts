import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004dPageRoutingModule } from './aitana004d-routing.module';

import { Aitana004dPage } from './aitana004d.page';

import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004dPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004dPage]
})
export class Aitana004dPageModule {}
