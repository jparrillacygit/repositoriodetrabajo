import { Component, OnInit } from '@angular/core';
import {MenuController} from '@ionic/angular';

@Component({
  selector: 'app-aitana004e',
  templateUrl: './aitana004e.page.html',
  styleUrls: ['./aitana004e.page.scss'],
})
export class Aitana004ePage implements OnInit {

  constructor( private menuCtrl:MenuController ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }

  

}
