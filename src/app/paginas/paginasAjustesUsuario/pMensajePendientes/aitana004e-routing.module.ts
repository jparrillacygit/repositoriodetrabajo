import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004ePage } from './aitana004e.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004ePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004ePageRoutingModule {}
