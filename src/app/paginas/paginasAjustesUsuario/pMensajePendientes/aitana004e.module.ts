import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004ePageRoutingModule } from './aitana004e-routing.module';

import { Aitana004ePage } from './aitana004e.page';

import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004ePageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004ePage]
})
export class Aitana004ePageModule {}
