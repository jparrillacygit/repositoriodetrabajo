import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004gPage } from './aitana004g.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004gPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004gPageRoutingModule {}
