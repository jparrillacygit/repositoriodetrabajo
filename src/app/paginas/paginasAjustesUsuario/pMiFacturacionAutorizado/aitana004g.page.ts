import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-aitana004g',
  templateUrl: './aitana004g.page.html',
  styleUrls: ['./aitana004g.page.scss'],
})
export class Aitana004gPage implements OnInit {

  constructor( private menuCtrl:MenuController ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }

}
