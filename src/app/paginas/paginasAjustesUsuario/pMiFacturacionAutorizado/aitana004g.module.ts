import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004gPageRoutingModule } from './aitana004g-routing.module';

import { Aitana004gPage } from './aitana004g.page';
import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004gPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004gPage]
})
export class Aitana004gPageModule {}
