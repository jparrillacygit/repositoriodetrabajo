import { Component, OnInit } from '@angular/core';
import {MenuController} from '@ionic/angular';

@Component({
  selector: 'app-aitana004j',
  templateUrl: './aitana004j.page.html',
  styleUrls: ['./aitana004j.page.scss'],
})
export class Aitana004jPage implements OnInit {

  constructor( private menuCtrl:MenuController ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }

}
