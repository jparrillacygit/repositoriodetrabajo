import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004jPage } from './aitana004j.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004jPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004jPageRoutingModule {}
