import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004jPageRoutingModule } from './aitana004j-routing.module';

import { Aitana004jPage } from './aitana004j.page';

import {ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004jPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004jPage]
})
export class Aitana004jPageModule {}
