import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004cPage } from './aitana004c.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004cPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004cPageRoutingModule {}
