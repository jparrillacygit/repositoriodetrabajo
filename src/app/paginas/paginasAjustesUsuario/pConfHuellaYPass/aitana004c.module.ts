import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004cPageRoutingModule } from './aitana004c-routing.module';

import { Aitana004cPage } from './aitana004c.page';

import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004cPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004cPage]
})
export class Aitana004cPageModule {}
