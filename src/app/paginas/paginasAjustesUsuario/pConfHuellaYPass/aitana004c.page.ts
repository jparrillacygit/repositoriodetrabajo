import { Component, OnInit } from '@angular/core';
import {MenuController} from '@ionic/angular'

@Component({
  selector: 'app-aitana004c',
  templateUrl: './aitana004c.page.html',
  styleUrls: ['./aitana004c.page.scss'],
})
export class Aitana004cPage implements OnInit {

  constructor( private menuCtrl:MenuController ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }

}
