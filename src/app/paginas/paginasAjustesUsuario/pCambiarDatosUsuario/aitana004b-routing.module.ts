import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004bPage } from './aitana004b.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004bPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004bPageRoutingModule {}
