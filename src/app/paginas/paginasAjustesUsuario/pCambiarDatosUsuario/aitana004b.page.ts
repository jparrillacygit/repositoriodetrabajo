import { Component, OnInit } from '@angular/core';
import { IonInput, MenuController } from '@ionic/angular';
import {ObtenerDatosUsuarioService} from '../../../servicios/usuario/get-data-user-service/obtener-datos-usuario.service';
import { InterfazUsuario} from '../../../servicios/interfaz-usuario';
import {ModificarUsuarioService} from '../../../servicios/usuario/modify-user-service/modificar-usuario.service';

@Component({
  selector: 'app-aitana004b',
  templateUrl: './aitana004b.page.html',
  styleUrls: ['./aitana004b.page.scss'],
})
export class Aitana004bPage implements OnInit {

  private usuario: InterfazUsuario;



  constructor( private menuCtrl:MenuController, private obtenerDatosUsuarioService: ObtenerDatosUsuarioService, private modificarUsuarioService: ModificarUsuarioService ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
    this.obtenerUsuario();
}



obtenerUsuario() {
  this.obtenerDatosUsuarioService.getUsuario().subscribe(
    (data) => {

      console.log(data);

      this.usuario = {
    
        id: data.id,
        username:  data.username,
        nombre:  data.nombre,
        apellido:  data.apellidos,
        email:  data.email,
        notificaciones_push:  data.notificaciones_push
      };
      
    },
    error => {
      console.log(error);
      
    }
  );

  this.usuario = {
    
    id: 0,
    username:  '',
    nombre:  '',
    apellido:  '',
    email:  '',
    notificaciones_push: 0
  };
}

  guardarCambiosUsuario(nombre: IonInput,apellidos: IonInput){

      this.modificarUsuarioService.modificarUsuario(this.usuario.id,nombre.value.toString(),apellidos.value.toString()).subscribe(
        (error) =>{
            console.log(error);
        }
      );


  }

}
