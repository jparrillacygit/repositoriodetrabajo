import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004bPageRoutingModule } from './aitana004b-routing.module';

import { Aitana004bPage } from './aitana004b.page';
import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004bPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004bPage]
})
export class Aitana004bPageModule {}
