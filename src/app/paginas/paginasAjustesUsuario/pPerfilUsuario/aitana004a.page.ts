import { Component, OnInit } from '@angular/core';
import { IonInput, IonToggle, MenuController } from '@ionic/angular';
import { ObtenerValoracionUsuarioService } from 'src/app/servicios/usuario/get-rating-service/obtener-valoracion-usuario.service';
import { AdmNotificacionService } from '../../../servicios/admin-notify/adm-notificacion.service';
import { ObtenerDatosUsuarioService } from '../../../servicios/usuario/get-data-user-service/obtener-datos-usuario.service';
import {InterfazUsuario} from '../../../servicios/interfaz-usuario';

@Component({
  selector: 'app-aitana004a',
  templateUrl: './aitana004a.page.html',
  styleUrls: ['./aitana004a.page.scss'],
})
export class Aitana004aPage implements OnInit {

  private usuario: InterfazUsuario;
  private startToggle:number=0;
  constructor( 
    private menuCtrl:MenuController,
    private notf:AdmNotificacionService, 
    private obtenerDatosUsuarioService : ObtenerDatosUsuarioService,
    private servValoracion:ObtenerValoracionUsuarioService
     ) { }
  private valoracion=2.5;
  private estrellas=[5];
  ngOnInit() {

    this.menuCtrl.enable(true);
    this.obtenerValoraciones();
    this.obtenerUsuario();
    
  }
  obtenerEstrellas(){
    let i:number=1;
    while(i<=5){
      if(this.valoracion>=i){
        
        this.estrellas.push(1);
      }else{
        if(i-this.valoracion==0.5){
          this.estrellas.push(0.5);
        }else{
          this.estrellas.push(0);
        }
      }
      i++;
    }
  }

  


  habilitarNotificacion(checkNotificacion:IonToggle){
    let eleccion=0;
    if(checkNotificacion.checked){
      eleccion=1;
    }
    
    console.log("Devuelve:")
    this.notf.habilitarNotificaciones(eleccion).subscribe(
      data => {
        
        console.log("Inicio");
      },
      error => {
        console.log(error);
        
      },
      () => {
        
      }
    )
  }
  obtenerUsuario() {
    this.obtenerDatosUsuarioService.getUsuario().subscribe(
      (data) => {
  
        console.log(data);
  
        this.usuario = {
      
          id: data.id,
          username:  data.username,
          nombre:  data.nombre,
          apellido:  data.apellido,
          email:  data.email,
          notificaciones_push:  data.notificaciones_push
        };
        this.startToggle=this.usuario.notificaciones_push;
        
      },
      error => {
        console.log(error);
        
      }
    );
  
    this.usuario = {
      
      id: 0,
      username:  '',
      nombre:  '',
      apellido:  '',
      email:  '',
      notificaciones_push: 0
    };
  }
  obtenerValoraciones() {
    
      
      this.servValoracion.getValoracionUsuario().subscribe(
        (data) => {
  
          console.log("EEEE: "+data);
          this.valoracion=data["valoracion"];
          this.obtenerEstrellas();
          
  
          
        },
        error => {
          console.log(error);
          
        }
      )
  }

  activarODesactivarNofificacion(toggle:IonToggle){
      let valor=1;
      if(!toggle.checked){
        valor=0;
        console.log(" NO ESTA PULSADO");
      }
      this.notf.habilitarNotificaciones(valor).subscribe(
        (data) => {
  
          console.log("REALIZADO CORRECTAMENTE : "+data);
          
          
  
          
        },
        error => {
          console.log(error);
          
        }
      );
  }
}




