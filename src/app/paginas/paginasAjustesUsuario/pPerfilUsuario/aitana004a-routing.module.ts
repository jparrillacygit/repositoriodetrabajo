import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004aPage } from './aitana004a.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004aPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004aPageRoutingModule {}
