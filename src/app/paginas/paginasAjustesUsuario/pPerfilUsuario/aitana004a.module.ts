import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004aPageRoutingModule } from './aitana004a-routing.module';

import { Aitana004aPage } from './aitana004a.page';
import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004aPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004aPage]
})
export class Aitana004aPageModule {}
