import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004hPageRoutingModule } from './aitana004h-routing.module';

import { Aitana004hPage } from './aitana004h.page';
import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004hPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004hPage]
})
export class Aitana004hPageModule {}
