import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004hPage } from './aitana004h.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004hPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004hPageRoutingModule {}
