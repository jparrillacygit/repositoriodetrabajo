import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-aitana004h',
  templateUrl: './aitana004h.page.html',
  styleUrls: ['./aitana004h.page.scss'],
})
export class Aitana004hPage implements OnInit {

  constructor( private menuCtrl:MenuController ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }

}
