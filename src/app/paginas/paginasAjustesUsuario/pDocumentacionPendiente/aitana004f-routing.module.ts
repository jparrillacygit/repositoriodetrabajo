import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana004fPage } from './aitana004f.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana004fPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana004fPageRoutingModule {}
