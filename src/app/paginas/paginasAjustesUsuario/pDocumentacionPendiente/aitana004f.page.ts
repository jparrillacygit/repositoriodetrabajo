import { Component, OnInit } from '@angular/core';
import {IonItem, MenuController} from '@ionic/angular'

@Component({
  selector: 'app-aitana004f',
  templateUrl: './aitana004f.page.html',
  styleUrls: ['./aitana004f.page.scss'],
})
export class Aitana004fPage implements OnInit {

  items : Item [] = [
    {
      icono1:'image-outline',
      titulo:'Foto técnico',
      icono2:'cloud-upload-outline',
      isClicked: false
      
    },
    {
      icono1:'document-text-outline',
      titulo:'Informe de Accidentalidad',
      icono2:'cloud-upload-outline',
      isClicked: false
    },
    {
      icono1:'document-text-outline',
      titulo:'Justificante de pago del Contrato',
      icono2:'cloud-upload-outline',
      isClicked: false
      
    },
    {
      icono1:'document-text-outline',
      titulo:'Modelo 036 de Hacienda',
      icono2:'cloud-upload-outline',
      isClicked: false
      
    },
    {
      icono1:'document-text-outline',
      titulo:'Evaluación de riesgos laborales',
      icono2:'cloud-upload-outline',
      isClicked: false
      
    },
    {
      icono1:'document-text-outline',
      titulo:'Recibo de autónomo',
      icono2:'cloud-upload-outline',
      isClicked: false
      
    },
    {
      icono1:'document-text-outline',
      titulo:'Reconocimiento Médico',
      icono2:'cloud-upload-outline',
      isClicked: false
      
    },
    {
      icono1:'image-outline',
      titulo:'DNI',
      icono2:'cloud-upload-outline',
      isClicked: false
      
    }
  ];

  constructor( private menuCtrl:MenuController ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }

  cambiarColor(item: Item){

    if (item.isClicked) {

      item.isClicked = false;

    } else {

      item.isClicked = true;
     
    }

  }

}

interface Item {
  icono1: string
  titulo: string
  icono2: string
  isClicked: boolean
}
