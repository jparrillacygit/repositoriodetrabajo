import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana004fPageRoutingModule } from './aitana004f-routing.module';

import { Aitana004fPage } from './aitana004f.page';

import { ComponentesModule } from '../../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana004fPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana004fPage]
})
export class Aitana004fPageModule {}
