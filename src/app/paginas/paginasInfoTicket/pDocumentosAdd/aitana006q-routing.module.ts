import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006qPage } from './aitana006q.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006qPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006qPageRoutingModule {}
