import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonImg, IonSelect } from '@ionic/angular';

@Component({
  selector: 'app-aitana006q',
  templateUrl: './aitana006q.page.html',
  styleUrls: ['./aitana006q.page.scss'],
})
export class Aitana006qPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  
  adjuntarFoto(imagen:IonImg,tipo:IonSelect){
    this.router.navigate(['/aitana006l']);
  }
}
