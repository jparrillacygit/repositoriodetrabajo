import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006qPageRoutingModule } from './aitana006q-routing.module';

import { Aitana006qPage } from './aitana006q.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006qPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006qPage]
})
export class Aitana006qPageModule {}
