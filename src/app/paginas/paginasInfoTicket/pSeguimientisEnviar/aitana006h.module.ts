import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006hPageRoutingModule } from './aitana006h-routing.module';

import { Aitana006hPage } from './aitana006h.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006hPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006hPage]
})
export class Aitana006hPageModule {}
