import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonTextarea, IonToggle, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-aitana006h',
  templateUrl: './aitana006h.page.html',
  styleUrls: ['./aitana006h.page.scss'],
})
export class Aitana006hPage implements OnInit {

  constructor(private router:Router,
    private menuCtrl:MenuController) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }
  enviarComentario(texto:IonTextarea,toggle:IonToggle){
    this.router.navigate(['/aitana006h']);
  }

}
