import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006hPage } from './aitana006h.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006hPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006hPageRoutingModule {}
