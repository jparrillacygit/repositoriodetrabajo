import { Component, OnInit ,Pipe,PipeTransform} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
@Component({
  selector: 'app-ejemplo',
  templateUrl: './ejemplo.page.html',
  styleUrls: ['./ejemplo.page.scss'],
})
export class EjemploPage implements OnInit {

  private latitud:number=20;
  private longitud:number=20;
 
  private idTicket:number;
  private urlSafe;
  constructor(private sanitizer: DomSanitizer,
    private activatedRoute:ActivatedRoute,
    private router:Router,
    private geolocation: Geolocation
    ) { }

  ngOnInit() {
    
    this.activatedRoute.paramMap.subscribe(
      paramMap =>{
        this.idTicket=Number(paramMap.get('idTicket'));
      }
    );
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitud = resp.coords.latitude;
      this.longitud = resp.coords.longitude;
      console.log("Coordenadas usuario: "+this.latitud +" ,"+this.longitud);
      this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl("https://maps.google.com/maps?q="+this.latitud+","+this.longitud+"&hl=es&z=14&amp&output=embed");
     }).catch((error) => {
       console.log('Error getting location', error);
     });
     this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl("https://maps.google.com/maps?q="+this.latitud+","+this.longitud+"&hl=es&z=14&amp&output=embed");
  }
}
