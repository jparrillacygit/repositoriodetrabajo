import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-aitana006k',
  templateUrl: './aitana006k.page.html',
  styleUrls: ['./aitana006k.page.scss'],
})
export class Aitana006kPage implements OnInit {

  constructor(private menuCtrl:MenuController) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
  }

}
