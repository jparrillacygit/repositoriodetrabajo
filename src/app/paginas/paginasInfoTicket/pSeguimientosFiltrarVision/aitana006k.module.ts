import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006kPageRoutingModule } from './aitana006k-routing.module';

import { Aitana006kPage } from './aitana006k.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006kPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006kPage]
})
export class Aitana006kPageModule {}
