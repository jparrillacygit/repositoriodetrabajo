import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006kPage } from './aitana006k.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006kPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006kPageRoutingModule {}
