import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006rPageRoutingModule } from './aitana006r-routing.module';

import { Aitana006rPage } from './aitana006r.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006rPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006rPage]
})
export class Aitana006rPageModule {}
