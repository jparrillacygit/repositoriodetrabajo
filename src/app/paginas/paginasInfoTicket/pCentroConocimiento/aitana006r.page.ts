import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aitana006r',
  templateUrl: './aitana006r.page.html',
  styleUrls: ['./aitana006r.page.scss'],
})
export class Aitana006rPage implements OnInit {
   archivos:{
    titulo:String,
    fecha:String,
    hora:String,
    marca:String,
    modelo:String,
    imagen:String
  }[]=[]
  
  constructor() { }

  ngOnInit() {
    this.cargarArchivos()
  }
  cargarArchivos() {
    this.archivos.push(
      {
        titulo:"ASUS VivoBook 14",
        fecha:"05/04/2018",
        hora:"14:56",
        marca:"ASUS",
        modelo:"VivoBook 14",
        imagen:"pdfImagen.png"
      },
      {
        titulo:"ASUS VivoBook Fip",
        fecha:"05/04/2018",
        hora:"14:35",
        marca:"ASUS",
        modelo:"VivoBook Fip",
        imagen:"pdfImagen.png"
      },
      {
        titulo:"ASUS TUF Gaming FX505",
        fecha:"05/04/2018",
        hora:"11:23",
        marca:"ASUS",
        modelo:"TUF Gaming FX505",
        imagen:"pdfImagen.png"
      },
      {
        titulo:"ASUS ROG Strix",
        fecha:"04/04/2018",
        hora:"13:45",
        marca:"ASUS",
        modelo:"ROG Strix",
        imagen:"pdfImagen.png"
      },
      {
        titulo:"ASUS ZenBook Flip 13",
        fecha:"04/04/2018",
        hora:"10:45",
        marca:"ASUS",
        modelo:"ZenBook Flip 13",
        imagen:"pdfImagen.png"
      },
    )
    console.log(this.archivos)
  }

}
