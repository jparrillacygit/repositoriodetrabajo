import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006rPage } from './aitana006r.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006rPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006rPageRoutingModule {}
