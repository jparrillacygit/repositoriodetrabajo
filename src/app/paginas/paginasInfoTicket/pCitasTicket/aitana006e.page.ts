import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import {ObtenerSeguimientoYCitaService} from '../../../servicios/tickets/token/obtener-seguimiento-ycita.service';
import {ActivatedRoute} from '@angular/router';
import {InterfazTicket} from '../../../servicios/interface-ticket';
import { Router } from '@angular/router';
import {CitasTickets} from '../../../servicios/citas-interfaz-ticket';
import {ObtenerTicketService } from '../../../servicios/tickets/get-ticket/obtener-ticket.service';


@Component({
  selector: 'app-aitana006e',
  templateUrl: './aitana006e.page.html',
  styleUrls: ['./aitana006e.page.scss'],
})
export class Aitana006ePage implements OnInit {

  constructor(private alertas:AlertController,private router: Router,private activateRouter : ActivatedRoute, private obtenerCitaService: ObtenerSeguimientoYCitaService, private obtenerTicket: ObtenerTicketService) { }

  id : number;
  idCita: number;

  stringFecha: string;
  stringTiempo: string;
  fecha: string;
  tiempo: string;
  momento: string;

   cliente: string;
   direccion: string;

  private ticket : InterfazTicket;
  private citaTicket: CitasTickets;
  private citasTickets: CitasTickets [] = [];

  ngOnInit() {

    this.activateRouter.paramMap.subscribe(
      paramMap =>{
      this.id = Number(paramMap.get('id'));
      }
    );

    this.getCitasTickets(this.id);
    this.getTicket(this.id);
  }


  getCitasTickets(id :number){

    this.obtenerCitaService.getDetallesTickets(id,1).subscribe(
      (data) =>{
        console.log(data);

        let json = JSON.parse(JSON.stringify(data));

        for (var i = 0; i< json.elements.length;i++){

            this.idCita = json.elements[i].ID;
            this.stringFecha = json.elements[i].fecha_cita;
            this.stringTiempo = json.elements[i].fecha_cita;

            this.fecha = this.stringFecha.slice(0,-14);

            this.tiempo = this.stringTiempo.slice(11,-8);

            this.momento =  this.fecha + " - " + this.tiempo;

            this.citaTicket = {
              id: this.idCita,
              fecha: this.momento
            }

            this.citasTickets.push(this.citaTicket);

        }


      },
      (error) =>{
        console.log(error);
      }
    )

  }


  getTicket(id: number){

    this.obtenerTicket.getTicket(id).subscribe(
      (data) =>{
        let json = JSON.parse(JSON.stringify(data));

        this.direccion = json.cliente.direccion;
        this.cliente = json.cliente.nombre;

        this.ticket = {
          direccion: this.direccion,
          cliente : this.cliente
        }


      },
      (error) =>{
        console.log(error);
      }
    )

  }

  async aitana006i(){
    const alert = await this.alertas.create({
      cssClass: 'colorFondo',
      
      message: '<ion-label>Crear cita</ion-label><br>Indique la fecha el dia y la hora',
      inputs:[
        
        {
         
        name: 'movil',
        type: 'date',
        label: 'movil',
        
      },],
      buttons: [{
        text: 'Guardar',
        cssClass: 'botonPrincipal',
        handler: () => {
          console.log('Confirm Cancel');
        }
      }, {
        text: 'Cancelar',
        cssClass:'botonSecondario',
        role: 'cancel',
        handler: () => {
          console.log('Confirm Ok');
        }
      }]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
  }
}
