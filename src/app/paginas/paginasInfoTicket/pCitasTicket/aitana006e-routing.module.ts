import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006ePage } from './aitana006e.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006ePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006ePageRoutingModule {}
