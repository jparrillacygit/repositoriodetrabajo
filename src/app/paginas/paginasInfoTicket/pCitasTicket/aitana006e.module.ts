import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006ePageRoutingModule } from './aitana006e-routing.module';

import { Aitana006ePage } from './aitana006e.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006ePageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006ePage]
})
export class Aitana006ePageModule {}
