import { Component, OnInit ,Pipe} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-aitana006d',
  templateUrl: './aitana006d.page.html',
  styleUrls: ['./aitana006d.page.scss'],
})
@Pipe({
  name: 'safe'
})
export class Aitana006dPage implements OnInit {
  private latitud:number=20;
  private longitud:number=20;
  private termino:boolean=false;
  private idTicket:number;
  constructor(private sanitizer: DomSanitizer,
    private activatedRoute:ActivatedRoute,
    private router:Router
    ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(
      paramMap =>{
        this.idTicket=Number(paramMap.get('idTicket'));
      }
    );

  }
  transform(safe?){
    this.activatedRoute.paramMap.subscribe(
      paramMap =>{
      this.latitud = Number(paramMap.get('latitud'));
      this.longitud = Number(paramMap.get('longitud'));
      this.idTicket=Number(paramMap.get('idTicket'));

      console.log("Ubicación");
      console.log(this.latitud);
      console.log(this.longitud);
      if( this.latitud==0 && this.longitud==0){
        this.router.navigate(['/aitana006a',this.idTicket]);
      }
      this.termino=true;
      return this.sanitizer.bypassSecurityTrustResourceUrl("https://maps.google.com/maps?q="+this.latitud+","+this.longitud+"&hl=es&z=14&amp&output=embed");
      }
    );
    return this.sanitizer.bypassSecurityTrustResourceUrl("https://maps.google.com/maps?q="+this.latitud+","+this.longitud+"&hl=es&z=14&amp&output=embed");
  }

}
