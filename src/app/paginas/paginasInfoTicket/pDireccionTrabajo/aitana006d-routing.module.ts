import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { App } from '@capacitor/core';

import { Aitana006dPage } from './aitana006d.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006dPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
  exports: [RouterModule],
})
export class Aitana006dPageRoutingModule {}
