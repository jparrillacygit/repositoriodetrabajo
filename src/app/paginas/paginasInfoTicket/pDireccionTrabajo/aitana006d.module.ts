import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006dPageRoutingModule } from './aitana006d-routing.module';

import { Aitana006dPage } from './aitana006d.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006dPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006dPage]
})
export class Aitana006dPageModule {}
