import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006lPage } from './aitana006l.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006lPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006lPageRoutingModule {}
