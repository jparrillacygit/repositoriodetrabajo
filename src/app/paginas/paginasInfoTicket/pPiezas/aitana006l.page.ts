import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import {ObtenerSeguimientoYCitaService} from '../../../servicios/tickets/token/obtener-seguimiento-ycita.service';
import {ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import {DiagnosisTickets} from '../../../servicios/interfaz-diagnosis-ticket';


@Component({
  selector: 'app-aitana006l',
  templateUrl: './aitana006l.page.html',
  styleUrls: ['./aitana006l.page.scss'],
})
export class Aitana006lPage implements OnInit {

  diagnosisTicket : DiagnosisTickets;
  diagnosisTickets: DiagnosisTickets [] = [];

  id: number;

  pinDiagnosis : string;

  fecha : string;

  horas: string;

  tiempo: string;


  constructor(private menuCtrl:MenuController,private router: Router, private activatedRouter : ActivatedRoute, private obtenerSeguimientoYCitaService : ObtenerSeguimientoYCitaService) { }

  ngOnInit() {
    this.menuCtrl.enable(true);

    this.activatedRouter.paramMap.subscribe(
      paramMap =>{
      this.id = Number(paramMap.get('id'));
      }
    );

    this.getDiagnosis(this.id);
  }

  getDiagnosis(id : number){
    this.obtenerSeguimientoYCitaService.getDetallesTickets(id,2).subscribe(
      (data) =>{
        console.log(data);

        let json = JSON.parse(JSON.stringify(data));

        for (var i = 0; i<json.elements.length;i++){
              this.pinDiagnosis = json.elements[i].diag_pn1;
               this.fecha = json.elements[i].datetime_creacion;
               this.horas = json.elements[i].datetime_creacion;

              this.tiempo = this.fecha.slice(0,10) + " " + this.horas.slice(11,16);

               this.diagnosisTicket = {

                 pin: this.pinDiagnosis,
                 fecha: this.tiempo
               }

               this.diagnosisTickets.push(this.diagnosisTicket);
        }
      },
      (error) =>{
        console.log(error);
      }
    )
  }

  

}
