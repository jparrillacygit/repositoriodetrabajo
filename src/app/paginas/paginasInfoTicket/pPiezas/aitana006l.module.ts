import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006lPageRoutingModule } from './aitana006l-routing.module';

import { Aitana006lPage } from './aitana006l.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006lPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006lPage]
})
export class Aitana006lPageModule {}
