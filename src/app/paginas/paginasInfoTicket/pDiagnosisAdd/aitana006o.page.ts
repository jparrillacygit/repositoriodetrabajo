import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonInput, IonSelect } from '@ionic/angular';

@Component({
  selector: 'app-aitana006o',
  templateUrl: './aitana006o.page.html',
  styleUrls: ['./aitana006o.page.scss'],
})
export class Aitana006oPage implements OnInit {

  constructor(private router:Router ) { }

  ngOnInit() {
    
    
  }
   //
   crearDiagnosis(opcion:IonSelect,pin:IonInput,cantidad:IonInput,descripcion:IonInput,almacen:IonInput){
    this.router.navigate(['/aitana006l']);
   }
}
