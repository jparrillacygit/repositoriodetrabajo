import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006oPageRoutingModule } from './aitana006o-routing.module';

import { Aitana006oPage } from './aitana006o.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006oPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006oPage]
})
export class Aitana006oPageModule {}
