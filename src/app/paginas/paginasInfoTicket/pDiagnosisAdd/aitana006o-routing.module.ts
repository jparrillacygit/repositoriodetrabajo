import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006oPage } from './aitana006o.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006oPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006oPageRoutingModule {}
