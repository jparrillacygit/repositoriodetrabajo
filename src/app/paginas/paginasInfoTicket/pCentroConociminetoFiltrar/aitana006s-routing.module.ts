import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006sPage } from './aitana006s.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006sPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006sPageRoutingModule {}
