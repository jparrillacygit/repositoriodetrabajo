import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006sPageRoutingModule } from './aitana006s-routing.module';

import { Aitana006sPage } from './aitana006s.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006sPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006sPage]
})
export class Aitana006sPageModule {}
