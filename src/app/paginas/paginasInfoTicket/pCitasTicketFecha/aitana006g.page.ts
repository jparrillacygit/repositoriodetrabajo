import { Component, OnInit } from '@angular/core';
import { ObtenerTicketService } from 'src/app/servicios/tickets/get-ticket/obtener-ticket.service';

@Component({
  selector: 'app-aitana006g',
  templateUrl: './aitana006g.page.html',
  styleUrls: ['./aitana006g.page.scss'],
})
export class Aitana006gPage implements OnInit {
  id:number;
  ticketObtenido:{
    id:number,
    correo:string,

    fecha:string,
    hora:string,
    direccion:string
  }={
    id:0,
    correo:"string",

    fecha:"string",
    hora:"string",
    direccion:"string"
  };
  constructor(private ticket:ObtenerTicketService) { }

  ngOnInit() {
      this.id=1207168;
      this.ticket.getTicket(1207168).subscribe(
        data=>{
          let datos=JSON.parse(JSON.stringify(data));
          this.ticketObtenido={
            id:this.id,
            correo:datos.cliente.email_contacto,
            fecha:this.obtenerFecha(datos.date+""),
            hora:this.getHora(datos.date),
            direccion:datos.direccion
          }
          console.log(data);

        }
      )
  }
  getHora(date: String): string {
    console.log("HORA: "+date.substr(11,2)+" MINUTOS: "+date.substr(14,2));
    return ""+date.substr(11,2)+":"+date.substr(14,2);
  }
  obtenerFecha(date: String): string {
    let mes=["ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic"];
    //2021-05-03T12:00:01.000Z
   
    let dia= [
      'dom',
      'lun',
      'mar',
      'mié',
      'jue',
      'vie',
      'sáb',
    ][new Date(date+"").getDay()];
    let fecha:string=dia+".,"+date.substr(8,2)+mes[Number(date.substr(5,2))]+"."+date.substr(0,4)
    console.log("FECHA EN BONITO: "+fecha+" DIA EN BONITO"+dia);

    return fecha;
  }

}
