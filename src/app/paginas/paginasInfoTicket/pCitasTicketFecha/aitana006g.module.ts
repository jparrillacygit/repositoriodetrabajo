import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006gPageRoutingModule } from './aitana006g-routing.module';

import { Aitana006gPage } from './aitana006g.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006gPageRoutingModule
  ],
  declarations: [Aitana006gPage]
})
export class Aitana006gPageModule {}
