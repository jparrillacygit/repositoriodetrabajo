import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006gPage } from './aitana006g.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006gPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006gPageRoutingModule {}
