import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006fPage } from './aitana006f.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006fPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006fPageRoutingModule {}
