import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006fPageRoutingModule } from './aitana006f-routing.module';

import { Aitana006fPage } from './aitana006f.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006fPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006fPage]
})
export class Aitana006fPageModule {}
