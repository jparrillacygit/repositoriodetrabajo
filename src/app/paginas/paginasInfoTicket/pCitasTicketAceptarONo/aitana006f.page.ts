import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {ObtenerSeguimientoYCitaService} from '../../../servicios/tickets/token/obtener-seguimiento-ycita.service';
import {CitasTickets} from '../../../servicios/citas-interfaz-ticket';
import {InterfazTicket} from '../../../servicios/interface-ticket';
import {ObtenerTicketService} from '../../../servicios/tickets/get-ticket/obtener-ticket.service';

@Component({
  selector: 'app-aitana006f',
  templateUrl: './aitana006f.page.html',
  styleUrls: ['./aitana006f.page.scss'],
})
export class Aitana006fPage implements OnInit {

  constructor(private menuCtrl:MenuController,private activatedRoute: ActivatedRoute,private obtenerSeguimientoYCitaService: ObtenerSeguimientoYCitaService,private obtenerTicketService: ObtenerTicketService) { }

  idTicket : number;
  idCita : number;

  fecha: string;
  tiempo: string;

  momento: string;

  direccion: string;
  cliente: string;

  private citaTicket: CitasTickets;
  private ticket : InterfazTicket;

  ngOnInit() {
    this.menuCtrl.enable(true);
    this.activatedRoute.paramMap.subscribe(
      paramMap =>{
      this.idTicket = Number(paramMap.get('id'));
      this.idCita = Number(paramMap.get('citaId'));
      console.log(this.idTicket);
      console.log(this.idCita);
      }
    );

    this.citaTicket = {
      id: 0,
      fecha: ''
    };

    this.ticket = {
      direccion: '',
      cliente : ''
    };

    this.getCitaTicket();
    this.getTicket(this.idTicket);
  }

  getCitaTicket(){

    this.obtenerSeguimientoYCitaService.getDetallesTickets(this.idTicket,1).subscribe(
      (data) =>{

        let json = JSON.parse(JSON.stringify(data));

        this.fecha = json.elements[this.idCita].fecha_cita;
        this.tiempo = json.elements[this.idCita].fecha_cita;

        this.momento = this.fecha.slice(0,-14) + " - " + this.tiempo.slice(11,-8);


        this.citaTicket = {
          id: this.idCita,
          fecha: this.momento
        }

      },
      (error) =>{

        console.log(error);

      }
    )


    
  }

  getTicket(id: number){

    this.obtenerTicketService.getTicket(this.idTicket).subscribe(
      (data) =>{
        let json = JSON.parse(JSON.stringify(data));

        this.direccion = json.cliente.direccion;
        this.cliente = json.cliente.nombre;

        this.ticket = {
          direccion: this.direccion,
          cliente : this.cliente
        }


      },
      (error) =>{
        console.log(error);
      }
    )

  }

}
