import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006jPage } from './aitana006j.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006jPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006jPageRoutingModule {}
