import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006jPageRoutingModule } from './aitana006j-routing.module';

import { Aitana006jPage } from './aitana006j.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006jPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006jPage]
})
export class Aitana006jPageModule {}
