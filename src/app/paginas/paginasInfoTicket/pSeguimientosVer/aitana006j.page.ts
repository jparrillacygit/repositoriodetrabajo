import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { ObtenerSeguimientoYCitaService } from 'src/app/servicios/tickets/token/obtener-seguimiento-ycita.service';

@Component({
  selector: 'app-aitana006j',
  templateUrl: './aitana006j.page.html',
  styleUrls: ['./aitana006j.page.scss'],
})
export class Aitana006jPage implements OnInit {
  private numTicket:number=0;
   private primerElementoSeguimiento:number=0;
  private numPagina:number=1;
  private id:number=0;
  seguimientos:{
    nick:String,
    fecha:String,
    titulo:String,
    descripcion:String,
  }[]=[];
  seguimientosTicket:{
    autor:String,
    fecha:String,
    contenido:String,
    
  }[]=[]
  constructor(private getSeguitiemtos:ObtenerSeguimientoYCitaService,private menuCtrl:MenuController,
    private activatedRouter:ActivatedRoute) { }

  ngOnInit() {
    this.activatedRouter.paramMap.subscribe(
      paramMap =>{
      this.id = Number(paramMap.get('id'));
      console.log("ID: "+this.id);

      }
    );
    this.obtenerSeguimientoYCitaService();
  }
  obtenerSeguimientoYCitaService(){
    
    this.menuCtrl.enable(true);
    //this.getDatos();
      //
      this.getSeguitiemtos.getDetallesTickets(this.id,0).subscribe(
        (data)=>{
          console.log("DATOS::");
          console.log(data);
          let json = JSON.parse(JSON.stringify(data));
          for (var i = 0; i<json.elements.length;i++){
                this.seguimientosTicket.push(
                  {
                    autor:json.elements[i].author,
                    fecha:json.elements[i].date,
                    contenido:json.elements[i].contents
                    
                  }
                )
            }
        },
        error=>{
          console.log("ERROR"+error);
        }
      );
      //console.log(this.tokenCita)
  }
  getDatos() {
    this.seguimientos.push(
      {
        nick:"@carlosalonso",
        fecha:"HOY 08:51",
        titulo:"Reparación portatil ASUS",
        descripcion:"Hace unos meses me ha empezado a aparecer unas rayas horizontales en la patalla cuando la muevo. Ahora me aprece inculo sin mover la pantalla"
      },
      {
        nick:"@carlosalonso",
        fecha:"AYER 09:47",
        titulo:"Reparación portatil ASUS",
        descripcion:"Hace unos meses me ha empezado a aparecer unas rayas horizontales en la patalla cuando la muevo. Ahora me aprece inculo sin mover la pantalla"
      },
      {
        nick:"@carlosalonso",
        fecha:"29/03/2021 09:47",
        titulo:"Reparación portatil ASUS",
        descripcion:"Hace unos meses me ha empezado a aparecer unas rayas horizontales en la patalla cuando la muevo. Ahora me aprece inculo sin mover la pantalla"
      },
    )
  }
  loadData(event){
      
    setTimeout(() => {
      console.log('Pagina:'+this.numPagina);
      
      this.cargarMasTickets(event);
      
    }, 500);
  }
  cargarMasTickets(event) {
    
    this.numTicket=0;
    console.log("PAGINA: "+this.numPagina)
    this.getSeguitiemtos.getDetallesTickets(this.numPagina,0,this.numPagina).subscribe(
      (data) =>{
        console.log("DATOS::");
          console.log(data);
          let addMas:boolean=true;
          let json = JSON.parse(JSON.stringify(data));
          if(this.seguimientosTicket[this.primerElementoSeguimiento].contenido==json.elements[0].contents){
            addMas=false;
            event.target.disabled=true;
          }
          for (var i = 0; i<json.elements.length;i++){
                if(addMas){
                  this.seguimientosTicket.push(
                    {
                      autor:json.elements[i].author,
                      fecha:json.elements[i].date,
                      contenido:json.elements[i].contents
                      
                    }
                  );
                }
                
            }
            if(json.elements.length!=10){
              event.target.disabled=true;
            }
            
            this.numPagina++;
      },
      (error) =>{
        console.log("ERROR EN 05A:");
        console.log(error);
      }
    );
  }
comprobarSalir(event: any) {
  if(this.numTicket!=10  ){
          
    event.target.disabled=true;
  }

  }
}
