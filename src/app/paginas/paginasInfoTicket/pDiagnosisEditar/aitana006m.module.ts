import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006mPageRoutingModule } from './aitana006m-routing.module';

import { Aitana006mPage } from './aitana006m.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006mPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006mPage]
})
export class Aitana006mPageModule {}
