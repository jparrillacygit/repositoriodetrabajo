import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonInput, IonSelect } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import {DiagnosisTickets} from '../../../servicios/interfaz-diagnosis-ticket';
import {ObtenerSeguimientoYCitaService} from '../../../servicios/tickets/token/obtener-seguimiento-ycita.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-aitana006m',
  templateUrl: './aitana006m.page.html',
  styleUrls: ['./aitana006m.page.scss'],
})
export class Aitana006mPage implements OnInit {

  id: number;
  ticketId: number;

  pinDiagnosis : string;
  descripcionDiagnosis : string;
  problemaDiagnosis : number;
  cantidad: string;
  almacenDiagnosis: string

  diagnosisTicket : DiagnosisTickets;

  constructor(private router:Router,private menuCtrl:MenuController, private activatedRouter : ActivatedRoute, private obtenerSeguimientoYCitaService : ObtenerSeguimientoYCitaService) { }

  ngOnInit() {
    this.activatedRouter.paramMap.subscribe(
      paramMap =>{
      this.id = Number(paramMap.get('id'));
      this.ticketId = Number(paramMap.get('ticketID'));

      }
    )
    this.diagnosisTicket = {
            pin: '',
            problema: 0,
            cantidad: '',
            almacenSap: '',
            descripcion: ''
    }

    this.getDiagnosisTicket();
   
  }


  getDiagnosisTicket(){
    this.obtenerSeguimientoYCitaService.getDetallesTickets(this.ticketId,2).subscribe(
      (data) =>{

          let json = JSON.parse(JSON.stringify(data));

          this.pinDiagnosis = json.elements[this.id].diag_pn1;
          this.descripcionDiagnosis = json.elements[this.id].diag_desc1;
          this.almacenDiagnosis = json.elements[this.id].almacen_sap;
          this.cantidad = json.elements[this.id].cantidadFactura;
          this.problemaDiagnosis = json.elements[this.id].problema1;


          if(this.descripcionDiagnosis == null || this.descripcionDiagnosis == " " ){

            this.descripcionDiagnosis = "-";

          }else if(this.almacenDiagnosis == null || this.almacenDiagnosis == " "){
            
            this.almacenDiagnosis = "-";

          }else if(this.cantidad == null || this.cantidad == " "){

              this.cantidad = "-";

          }else if(this.problemaDiagnosis == null){

              this.problemaDiagnosis = 0;
          }


          this.diagnosisTicket = {
            pin: this.pinDiagnosis,
            problema: this.problemaDiagnosis,
            cantidad: this.cantidad,
            almacenSap: this.almacenDiagnosis,
            descripcion: this.descripcionDiagnosis
          }

          console.log(this.diagnosisTicket);

          
      },
      (error) =>{
        console.log(error);
      }
    )
  } 

  actualizarValores(opcion:IonSelect,pin:IonInput,cantidad:IonInput,descripcion:IonInput,almacen:IonInput){

    this.router.navigate(['/aitana006l']);
  }
}
