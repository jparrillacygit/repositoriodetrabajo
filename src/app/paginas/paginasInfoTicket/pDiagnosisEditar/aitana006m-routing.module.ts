import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006mPage } from './aitana006m.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006mPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006mPageRoutingModule {}
