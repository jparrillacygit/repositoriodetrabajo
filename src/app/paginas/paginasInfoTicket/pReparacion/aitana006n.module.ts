import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006nPageRoutingModule } from './aitana006n-routing.module';

import { Aitana006nPage } from './aitana006n.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006nPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006nPage]
})
export class Aitana006nPageModule {}
