import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006nPage } from './aitana006n.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006nPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006nPageRoutingModule {}
