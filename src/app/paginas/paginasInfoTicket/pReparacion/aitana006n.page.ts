import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonInput } from '@ionic/angular';

@Component({
  selector: 'app-aitana006n',
  templateUrl: './aitana006n.page.html',
  styleUrls: ['./aitana006n.page.scss'],
})
export class Aitana006nPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  //this.router.navigate(['/aitana006l']);
  actualizarPieza(pnEntrega:IonInput,nsEntrega:IonInput,pnRetirado:IonInput,nsRetirado:IonInput){
    this.router.navigate(['/aitana006l']);
  }
}
