import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006aPageRoutingModule } from './aitana006a-routing.module';

import { Aitana006aPage } from './aitana006a.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006aPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006aPage]
})
export class Aitana006aPageModule {}
