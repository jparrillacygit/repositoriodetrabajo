import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, MenuController } from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {InterfazTicket} from '../../../servicios/interface-ticket';
import { ObtenerTicketService } from 'src/app/servicios/tickets/get-ticket/obtener-ticket.service';

import { CallNumber } from '@ionic-native/call-number/ngx';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { Geolocation } from '@ionic-native/geolocation/ngx';
@Component({
  selector: 'app-aitana006a',
  templateUrl: './aitana006a.page.html',
  styleUrls: ['./aitana006a.page.scss'],
})
export class Aitana006aPage implements OnInit {
    id : number;
    longitudDescripcion:number;
    esDescripcionCorta:boolean=true;
    private ticket : InterfazTicket;
    

  constructor(private alertas:AlertController,
    private route:Router,private activateRouter : ActivatedRoute, 
    private obtenerTicketService: ObtenerTicketService,
    private menuCtrl:MenuController,
    private callNumber: CallNumber,
    
    private socialSharing: SocialSharing,
    private router:Router,
    private geolocation: Geolocation
    ) { }

  ngOnInit() {
    //El 0.25 sale de la relación de que a 90 caracteres en una resolución de 360 salia 3 lineas
    // 90/360=0.25
    this.longitudDescripcion= 0.25*screen.width
    console.log("Longitud p"+screen.width)
    this.menuCtrl.enable(true);
    this.activateRouter.paramMap.subscribe(
      paramMap =>{
      this.id = Number(paramMap.get('id'));
      }
    );

    this.getTicket(this.id);
    
  }

  getTicket(id : number){

    this.ticket = {
      titulo: '',
      id: 105381,
      direccion: '',
      cliente: '',
      estado: '',
      tiempo: '',
      momento : '',
      telefono: '',
      movil: '',
      idProducto: '4398432098342',
      marcaProducto: 'BUITONIIIII',
      modeloProducto: 'AWAAAAAAA',
      descripcionCorta: '',
      descripcionLarga:'',
      latitud: '10',
      longitud: '20',
    }
    this.comprobarDescripcion();
    this.obtenerTicketService.getTicket(id).subscribe(
      (data) =>{
        console.log(data);

        let json = JSON.parse(JSON.stringify(data));
        
        let tiempo :string;
        let stringTiempo : string;
        let momentoActual = new Date();
        let momentoRecogido = new Date();
        let momento : string;
        
        let nombre: string="";
        let apellidos: string="";
        let cliente: string="";
        


        
        id = json.id;
        
        stringTiempo = json.date;
        
        
        

          tiempo = stringTiempo.slice(11,-8);

          momentoRecogido = new Date(json.date);

          var diferencia = Math.abs(momentoActual.getTime() - momentoRecogido.getTime());

          var diferenciaDias = Math.floor(diferencia / (1000 * 3600 * 24)); 

          if(diferenciaDias == 0){
              momento = 'Hoy';
          }else if (diferenciaDias == 1){
              momento = 'Mañana';
          }else{
              momento = stringTiempo.slice(0,-14);
          }
          if(json.cliente!=null){
            nombre = json.cliente.nombre+" ";
            apellidos = json.cliente.apellido,json.cliente+"";
        
            cliente = nombre + " "+ apellidos;
            this.ticket = {
              titulo: json.name+"",
              id: json.id,
              direccion: json.cliente.direccion+"",
              cliente: cliente,
              estado: this.datoNulo(json.estado+""),
              tiempo: tiempo,
              momento : momento,
              telefono: json.cliente.tlf_contacto+"",
              movil: this.datoNulo(json.cliente.movil_contacto)+"",
              idProducto: json.numserie+"",
              marcaProducto: json.marca+"",
              modeloProducto: json.facturable+"",
              descripcionCorta: this.datoNulo(json.modelo)+"",
              latitud: this.datoNulo(json.cliente.latitud) +"",
              longitud: this.datoNulo(json.cliente.longitud)+"",
            }
          }else{
            this.ticket = {
              titulo: json.name+"",
              id: json.id,
              direccion: "",
              cliente: cliente,
              estado: this.datoNulo(json.estado+""),
              tiempo: tiempo,
              momento : momento,
              telefono: "",
              movil: "",
              idProducto: json.numserie+"",
              marcaProducto: json.marca+"",
              modeloProducto: json.facturable+"",
              descripcionCorta: json.modelo+"",
              latitud: "",
              longitud: "",
            }
          }
          

          console.log(this.ticket);
          
      },
      (error) =>{
        console.log(error);
      }
    )

  }
  datoNulo(palabra:string):string{
    if(palabra==null){
      palabra='';
    }else if(palabra=='undefined'){
      palabra='';
    }
    return palabra;
  }
  comprobarDescripcion() {
    
    if(this.ticket.descripcionCorta.length>this.longitudDescripcion){
      
      this.ticket.descripcionLarga=this.ticket.descripcionCorta;
      this.ticket.descripcionCorta=this.ticket.descripcionCorta.substr(0,this.longitudDescripcion)+"...";
      
    }
  }
  
  botonUbicaciones(eleccion:number){
    switch(eleccion){
      case 1:this.aitana006b();break;
      case 2:this.aitana006c();break;
      
    }
  }
  async aitana006b(){
    if(this.ticket.movil!=''){
      const alert = await this.alertas.create({
        cssClass: 'colorFondo',
        
        message: '<b>¿Deseas compartir tu ubicación al cliente mediante Whatsap?</b>',
        buttons: [
          {
            text: 'Confirmar',
            cssClass: 'botonPrincipal',
            handler: (data) => {
              this.compartirUbicacion();
              
              
            }
          }, {
            text: 'Cancelar',
            cssClass:'botonSecondario',
            role: 'cancel',
            handler: () => {
              console.log('Confirm Ok');
            }
          }
        ]
      });
  
      await alert.present();
  
      const { role } = await alert.onDidDismiss();
      console.log('onDidDismiss resolved with role', role);
    }
    
  }
  compartirUbicacion() {
      
      this.geolocation.getCurrentPosition().then((resp) => {
        let latitud = resp.coords.latitude;
        let longitud = resp.coords.longitude;
        
        console.log("Coordenadas usuario: "+latitud +" ,"+longitud);
        // 'Ubicación del cliente'+'\n'+"https://maps.google.com/maps?q="+latitud+","+longitud+"&hl=es&z=14&amp&output=embed"
        this.socialSharing.shareViaWhatsAppToPhone('34'+this.ticket.movil,
        'Ubicación: \n',
        '',
        "https://maps.google.com/maps?q="+latitud+","+longitud+"&hl=es&z=14&amp&output=embed"
        )
       }).catch((error) => {
         console.log('Error getting location', error);
       });

      

  }
  async aitana006c(){
    console.log("ESTOY EN "+window.location);
    
    if(this.ticket.telefono=="" && this.ticket.movil==""){

    }else{
      
    const alert = await this.alertas.create({
      cssClass: 'colorFondo',
      
      message: '<ion-label >Selecciona un teléfono</ion-label>',
      inputs: this.addInputs(),
      buttons: [
        {
          text: 'Confirmar',
          cssClass: 'botonPrincipal',
          handler: (data) => {
            
            this.callNumber.callNumber(data, true)
              .then(res => console.log('Launched dialer!', res))
              .catch(err => console.log('Error launching dialer', err));
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Cancelar',
          cssClass:'botonSecondario',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    
      await alert.present();

      const { role } = await alert.onDidDismiss();
      console.log('onDidDismiss resolved with role', role);
    }
  }
  addInputs(): any {
    let insputs: {
      name: String,
      type: String,
      label: String,
      value: String,
      

    }[]=[];
    if(this.ticket.telefono!=""){
      insputs.push({
        name: 'telefono',
        type: 'radio',
        label: this.ticket.telefono+'',
        value: '954781245',
      })
      
    }
    
    if(this.ticket.movil==""   ){
      
      insputs.push(
        {
          name: 'movil',
          type: 'radio',
          label: this.ticket.movil+'',
          value: '645789412',
          
        }
      )
    }
   return insputs;
        
  }
  mostrarDescripcionDetallada(){
    if(this.esDescripcionCorta){
      if(this.ticket.descripcionLarga!=''){
        this.esDescripcionCorta=false;
      }
    }else{
      this.esDescripcionCorta=true;
    }
    
  }
  
}
