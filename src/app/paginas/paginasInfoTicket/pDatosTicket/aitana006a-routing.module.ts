import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CallNumber } from '@ionic-native/call-number/ngx';


import { Aitana006aPage } from './aitana006a.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006aPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[CallNumber]
  
})
export class Aitana006aPageRoutingModule {}
