import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana006pPageRoutingModule } from './aitana006p-routing.module';

import { Aitana006pPage } from './aitana006p.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana006pPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana006pPage]
})
export class Aitana006pPageModule {}
