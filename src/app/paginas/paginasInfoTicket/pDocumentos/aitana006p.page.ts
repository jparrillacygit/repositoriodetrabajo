import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aitana006p',
  templateUrl: './aitana006p.page.html',
  styleUrls: ['./aitana006p.page.scss'],
})
export class Aitana006pPage implements OnInit {
  documentos:{
    titulo:String,
    tipo:String,
    nick:String,
    fecha:String
  }[]=[];
  constructor() { }

  ngOnInit() {
    this.documentos.push(
      {
        titulo:"1617698293591.jpg",
        tipo:"Imagen/Fotografia",
        nick:"@carlosalonso",
        fecha:"06/04/2021 10:38"
    }
      );
  }
 
}
