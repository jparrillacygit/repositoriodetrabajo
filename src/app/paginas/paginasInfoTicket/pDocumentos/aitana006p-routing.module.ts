import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana006pPage } from './aitana006p.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana006pPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana006pPageRoutingModule {}
