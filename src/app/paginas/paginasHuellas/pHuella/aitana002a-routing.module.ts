import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana002aPage } from './aitana002a.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana002aPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana002aPageRoutingModule {}
