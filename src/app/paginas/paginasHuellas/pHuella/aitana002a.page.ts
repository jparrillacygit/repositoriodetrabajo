import { Component, OnInit } from '@angular/core';
import { ObtenerDatosUsuarioService } from '../../../servicios/usuario/get-data-user-service/obtener-datos-usuario.service';
import {InterfazUsuario} from '../../../servicios/interfaz-usuario';

@Component({
  selector: 'app-aitana002a',
  templateUrl: './aitana002a.page.html',
  styleUrls: ['../../../estiloComun/estilosComun001.scss','./aitana002a.page.scss'],
})
export class Aitana002aPage implements OnInit {

  private usuario: InterfazUsuario;

  constructor(private obtenerDatosUsuarioService: ObtenerDatosUsuarioService ) { }

  ngOnInit() {
    this.obtenerUsuario();
  }

  obtenerUsuario() {
    this.obtenerDatosUsuarioService.getUsuario().subscribe(
      (data) => {
  
        console.log(data);
  
        this.usuario = {
      
          id: data.id,
          username:  data.username,
          nombre:  data.nombre,
          apellido:  data.apellido,
          email:  data.email,
          notificaciones_push:  data.notificaciones_push
        };
        
      },
      error => {
        console.log(error);
        
      }
    );
  
    this.usuario = {
      
      id: 0,
      username:  '',
      nombre:  '',
      apellido:  '',
      email:  '',
      notificaciones_push: 0
    };
  }

}
