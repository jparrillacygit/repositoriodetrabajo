import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana002aPageRoutingModule } from './aitana002a-routing.module';

import { Aitana002aPage } from './aitana002a.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana002aPageRoutingModule,
    
  ],
  declarations: [Aitana002aPage]
})
export class Aitana002aPageModule {}
