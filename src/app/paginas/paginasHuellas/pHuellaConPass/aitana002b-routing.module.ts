import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana002bPage } from './aitana002b.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana002bPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana002bPageRoutingModule {}
