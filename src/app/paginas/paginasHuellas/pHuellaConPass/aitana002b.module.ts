import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana002bPageRoutingModule } from './aitana002b-routing.module';

import { Aitana002bPage } from './aitana002b.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana002bPageRoutingModule
  ],
  declarations: [Aitana002bPage]
})
export class Aitana002bPageModule {}
