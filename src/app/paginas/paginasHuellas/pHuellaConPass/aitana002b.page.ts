import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ObtenerDatosUsuarioService } from '../../../servicios/usuario/get-data-user-service/obtener-datos-usuario.service';
import {InterfazUsuario} from '../../../servicios/interfaz-usuario';

@Component({
  selector: 'app-aitana002b',
  templateUrl: './aitana002b.page.html',
  styleUrls: ['./aitana002b.page.scss','../../../estiloComun/estilosComun001.scss'],
})
export class Aitana002bPage implements OnInit {

  private usuario: InterfazUsuario;

  showPassword = false;
  iconoPassword = 'eye';

  constructor(private router:Router, private obtenerDatosUsuarioService : ObtenerDatosUsuarioService ) { }

  ngOnInit() {
    this.obtenerUsuario();
  }

  mostrarPassword(): void {
    this.showPassword = !this.showPassword;

    if(this.iconoPassword == 'eye'){
      
      this.iconoPassword = 'eye-off';

    }else{
      this.iconoPassword = 'eye';
    }
    
  }
  iniciarSesion(pass:HTMLInputElement){
    this.router.navigate(['/aitana005a'])
  }

  obtenerUsuario() {
    this.obtenerDatosUsuarioService.getUsuario().subscribe(
      (data) => {
  
        console.log(data);
  
        this.usuario = {
      
          id: data.id,
          username:  data.username,
          nombre:  data.nombre,
          apellido:  data.apellido,
          email:  data.email,
          notificaciones_push:  data.notificaciones_push
        };
        
      },
      error => {
        console.log(error);
        
      }
    );
  
    this.usuario = {
      
      id: 0,
      username:  '',
      nombre:  '',
      apellido:  '',
      email:  '',
      notificaciones_push: 0
    };
  }

}
