import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana005cPageRoutingModule } from './aitana005c-routing.module';

import { Aitana005cPage } from './aitana005c.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana005cPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana005cPage]
})
export class Aitana005cPageModule {}
