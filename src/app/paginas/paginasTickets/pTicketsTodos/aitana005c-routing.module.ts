import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CallNumber } from '@ionic-native/call-number/ngx';

import { Aitana005cPage } from './aitana005c.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana005cPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[CallNumber]
})
export class Aitana005cPageRoutingModule {}
