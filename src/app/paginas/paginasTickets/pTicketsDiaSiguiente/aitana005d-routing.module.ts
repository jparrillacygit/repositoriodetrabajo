import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CallNumber } from '@ionic-native/call-number/ngx';

import { Aitana005dPage } from './aitana005d.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana005dPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[CallNumber]
})
export class Aitana005dPageRoutingModule {}
