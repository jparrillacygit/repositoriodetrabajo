import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana005dPageRoutingModule } from './aitana005d-routing.module';

import { Aitana005dPage } from './aitana005d.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana005dPageRoutingModule,
    ComponentesModule
  ],
  declarations: [Aitana005dPage]
})
export class Aitana005dPageModule {}
