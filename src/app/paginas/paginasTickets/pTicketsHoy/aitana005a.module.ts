import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { Aitana005aPageRoutingModule } from './aitana005a-routing.module';

import { Aitana005aPage } from './aitana005a.page';
import { ComponentesModule } from '../../../componentes/componentes.module';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana005aPageRoutingModule,
    ComponentesModule
    
  ],
  declarations: [Aitana005aPage]
})
export class Aitana005aPageModule {}
