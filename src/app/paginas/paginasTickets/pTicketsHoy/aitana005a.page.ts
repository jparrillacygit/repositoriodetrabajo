import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {IonInfiniteScroll, IonInfiniteScrollContent, MenuController} from '@ionic/angular'
import {ObtenerTicketsService} from '../../../servicios/tickets/get-tickets/obtener-tickets.service';
import {ObtenerTicketService} from '../../../servicios/tickets/get-ticket/obtener-ticket.service';
import {InterfazTicket} from '../../../servicios/interface-ticket';

import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-aitana005a',
  templateUrl: './aitana005a.page.html',
  styleUrls: ['./aitana005a.page.scss'],
})
export class Aitana005aPage implements OnInit {

  private ticket : InterfazTicket;
  private tickets: InterfazTicket [] = [];
  private numTicket:number=0;
  private cargarMas:boolean=true;
  private cantTicket:number=0;
  private primerElementoPagina=0;
  private numPagina:number=0;
  


  constructor( private menuCtrl:MenuController, private route:Router,
    private obtenerTicketsService: ObtenerTicketsService,private obtenerTicketService : ObtenerTicketService,
    private callNumber: CallNumber,
    private activateRouter:ActivatedRoute ) { }

  ngOnInit() {
    
    this.menuCtrl.enable(true);
    this.obtenerTickets();
    
  }
  cargarMasTickets(event) {
    
    this.numTicket=0;
    let fecha :Date=new Date(Date.now());
    this.obtenerTicketsService.getTickets(this.numPagina,fecha.getFullYear()+"/"+fecha.getMonth() +"/" + fecha.getDate()).subscribe(
      (data) =>{
        console.log("MAS PAGINAS EN  05A");
        console.log(data);

        let json = JSON.parse(JSON.stringify(data));
        
        this.addTicketArray(json);
        
        
        event.target.complete();
        if(!this.cargarMas){
          event.target.disabled=true;
        }
      },
      (error) =>{
        console.log("ERROR EN 05A:");
        console.log(error);
      }
    );

    this.ticket = {
    
      titulo: '',
      id:  0,
      direccion:  '',
      estado:  '',
      tiempo:  '',
      momento: ''
    }; 
  }
  
  loadData(event){
    
    setTimeout(() => {
      console.log('Pagina:'+this.numPagina);
      
      this.cargarMasTickets(event);
      
    }, 500);
  }
  obtenerTickets(){
    
   this.numTicket=0;
   let fecha :Date=new Date(Date.now());

   console.log("Fecha actual:"+ fecha.getDate()+" :DIA "+fecha.getMonth() +" :MES" + fecha.getFullYear()+" año")
    this.obtenerTicketsService.getTickets(0,fecha.getFullYear()+"/"+fecha.getMonth() +"/" + fecha.getDate()).subscribe(
      (data) =>{
        console.log("BIEN EN 05A");
        console.log(data);

        let json = JSON.parse(JSON.stringify(data));
       
        this.addTicketArray(json)
        
       
      },
      (error) =>{
        console.log("ERROR EN 05A:");
        console.log(error);
      }
    );

    this.ticket = {
    
      titulo: '',
      id:  0,
      direccion:  '',
      estado:  '',
      tiempo:  '',
      momento: ''
    }; 
  }
  fechaActual() {
    let fecha 
  }
  


  getTicket(id : number){

    this.obtenerTicketService.getTicket(id).subscribe(
      (data) =>{
        console.log(data);
      },
      (error) =>{
        console.log(error);
      }
    )

  }
  addTicketArray(json: any) {
    let direccion="";
          this.cantTicket=json.totalElements;
          let tiempo;
          let telefono="";
          let stringTiempo : string;
          
          let momentoRecogido = new Date();
          let momento : string;
          
          this.cargarMas=json.hasMore;
          this.numPagina++;
          
          
          for (var i = 0;i< json.elements.length;i++){
            
            
            if(json.elements[i].cliente!=null){
              direccion=json.elements[i].cliente.direccion;
              telefono=json.elements[i].cliente.tlf_contacto;
            }
            stringTiempo = json.elements[i].date;
            tiempo = stringTiempo.slice(11,-8);
            momentoRecogido = new Date(json.elements[i].date);
            momento=json.elements[i].date.slice(0,10);    
                
                this.tickets.push( {
                  titulo: json.elements[i].name,
                  id: json.elements[i].id,
                  direccion: direccion,
                  estado: json.elements[i].facturable,
                  tiempo: tiempo,
                  momento: 'Hoy',
                  telefono:telefono
                })
                
          }
  }
  
  rutaTicket(eleccion:number){
    switch(eleccion){
      case 1:this.route.navigate(['/aitana005a']);break;
      case 2:this.route.navigate(['/aitana005c']);break;
      case 3:this.route.navigate(['/aitana005d']);break;
    }
  }
  llamarContacto(telefono:string){
    
    this.callNumber.callNumber(telefono, true)
              .then(res => console.log('Launched dialer!', res))
              .catch(err => console.log('Error launching dialer', err));
            console.log('Confirm Cancel');
  }
  buscarTicket(){
    
  }
}
