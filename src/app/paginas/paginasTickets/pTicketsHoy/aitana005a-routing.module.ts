import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana005aPage } from './aitana005a.page';
import { CallNumber } from '@ionic-native/call-number/ngx';
const routes: Routes = [
  {
    path: '',
    component: Aitana005aPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[CallNumber]
})
export class Aitana005aPageRoutingModule {}
