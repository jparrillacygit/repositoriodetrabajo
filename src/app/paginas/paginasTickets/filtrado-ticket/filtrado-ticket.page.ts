import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonInput, ToastController } from '@ionic/angular';
import { ObtenerTicketService } from 'src/app/servicios/tickets/get-ticket/obtener-ticket.service';

@Component({
  selector: 'app-filtrado-ticket',
  templateUrl: './filtrado-ticket.page.html',
  styleUrls: ['./filtrado-ticket.page.scss'],
})
export class FiltradoTicketPage implements OnInit {
  private ruta :string ="'aitana005a'"
  constructor(
    private getTicket:ObtenerTicketService,
    private activateRoute:ActivatedRoute,
    private router:Router,
    private toastController:ToastController
  ) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(
      paramMap =>{
        this.ruta= paramMap.get('ruta')//es el path que le habiamos asignado
        console.log("ruta:"+this.ruta)
        
      }
    )
  }
  buscarTicket(input:IonInput){
    console.log("Ticket:"+input.value+"")
    this.getTicket.getTicket(Number.parseInt(input.value+"")).subscribe(
      (data) =>{
        console.log(data);
        this.router.navigate(['/aitana006a',Number.parseInt(input.value+"")]);
      },
      (error) =>{
        console.log("ERROR NO EXISTE EL TICKET");
        this.lanzarError();
        
      }
    )
  }
  async lanzarError() {
    const toast = await this.toastController.create({
      duration:4000,
      cssClass:"error",
      message: "ERROR NOE EXISTE EL TICKET",
      position: 'bottom',

      
    });
    await toast.present();
  }
}
