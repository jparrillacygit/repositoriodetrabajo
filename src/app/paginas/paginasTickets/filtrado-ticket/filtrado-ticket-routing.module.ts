import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FiltradoTicketPage } from './filtrado-ticket.page';

const routes: Routes = [
  {
    path: '',
    component: FiltradoTicketPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FiltradoTicketPageRoutingModule {}
