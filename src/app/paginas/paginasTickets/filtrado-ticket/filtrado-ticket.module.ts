import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FiltradoTicketPageRoutingModule } from './filtrado-ticket-routing.module';

import { FiltradoTicketPage } from './filtrado-ticket.page';
import { ComponentesModule } from 'src/app/componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FiltradoTicketPageRoutingModule,
    ComponentesModule
  ],
  declarations: [FiltradoTicketPage]
})
export class FiltradoTicketPageModule {}
