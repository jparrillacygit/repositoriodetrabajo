import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana00bPage } from './aitana00b.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana00bPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana00bPageRoutingModule {}
