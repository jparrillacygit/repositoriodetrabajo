import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana00bPageRoutingModule } from './aitana00b-routing.module';

import { Aitana00bPage } from './aitana00b.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana00bPageRoutingModule
  ],
  declarations: [Aitana00bPage]
})
export class Aitana00bPageModule {}
