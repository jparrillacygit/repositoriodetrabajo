import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana003bPageRoutingModule } from './aitana003b-routing.module';

import { Aitana003bPage } from './aitana003b.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana003bPageRoutingModule
  ],
  declarations: [Aitana003bPage]
})
export class Aitana003bPageModule {}
