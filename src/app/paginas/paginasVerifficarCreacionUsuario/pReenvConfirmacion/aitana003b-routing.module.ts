import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana003bPage } from './aitana003b.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana003bPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana003bPageRoutingModule {}
