import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Aitana003aPageRoutingModule } from './aitana003a-routing.module';

import { Aitana003aPage } from './aitana003a.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Aitana003aPageRoutingModule
  ],
  declarations: [Aitana003aPage]
})
export class Aitana003aPageModule {}
