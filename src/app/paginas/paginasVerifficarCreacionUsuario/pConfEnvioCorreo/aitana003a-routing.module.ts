import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Aitana003aPage } from './aitana003a.page';

const routes: Routes = [
  {
    path: '',
    component: Aitana003aPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Aitana003aPageRoutingModule {}
