import { Injectable } from '@angular/core';
import { Aitana005bComponent } from '../componentes/menu/aitana005b.component';

@Injectable({
  providedIn: 'root'
})
export class ComunicadorService {
  private menu:Aitana005bComponent;

  constructor() { }
   setAitana005bComponent(aitana005b:Aitana005bComponent){
    this.menu=aitana005b;
  }
   getMenu():Aitana005bComponent{
    return this.menu;
  }
}
