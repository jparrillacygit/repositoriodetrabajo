import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BorrarDocumentoIncidenciasTransporteService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }

  elimnarDocumentoIncidenciaTransporte(id: number){

    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });

    return this.http.post<any>(this.env.API_URL + '/transporteIncidencias/deleteFile/'+ id, { headers: headers})
    .pipe(
      tap(documentosEliminados => {
        
        return documentosEliminados;
        
      })
    )

  }
}
