import { TestBed } from '@angular/core/testing';

import { BorrarDocumentoIncidenciasTransporteService } from './borrar-documento-incidencias-transporte.service';

describe('BorrarDocumentoIncidenciasTransporteService', () => {
  let service: BorrarDocumentoIncidenciasTransporteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BorrarDocumentoIncidenciasTransporteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
