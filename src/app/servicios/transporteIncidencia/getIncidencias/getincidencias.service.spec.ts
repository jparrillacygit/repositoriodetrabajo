import { TestBed } from '@angular/core/testing';

import { GetincidenciasService } from './getincidencias.service';

describe('GetincidenciasService', () => {
  let service: GetincidenciasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetincidenciasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
