import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';
import {HttpParams} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GetincidenciasService {

  constructor(private http: HttpClient,private env: EnvServiciosService) { }

  getIncidenciasTransportes(page? : number, pageSize? : number, fechaInicioGestion?: String,fechaFinGestion?:string,
    numExpediente?:string,tipo?:number,estado_global?:number,estado_seguro?:number){

    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    let title:any={ headers: headers };
    if(page!=null){
      const params = new HttpParams().set('page',page.toString())
        .set('page',page.toString())
        .set('pageSize',pageSize.toString())
        .set('fechaInicioGestion', fechaInicioGestion.toString())
        .set('fechaFinGestion', fechaFinGestion.toString())
        .set('numExpediente', numExpediente.toString())
        .set('tipo', tipo.toString())
        .set('estado_global', estado_global.toString())
        .set('estado_seguro', estado_seguro.toString());
      title={ headers: headers , params: params}
    }
    
    
  
      return this.http.get<any>(this.env.API_URL + '/transporteIncidencias', title)
      .pipe(
        tap(contratos => {
          
          return contratos;
        })
      )

  }
  getUnaIncidenciaTransporte(id:number){
    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    return this.http.get<any>(this.env.API_URL + '/transporteIncidencias/'+id, { headers: headers})
      .pipe(
        tap(contratos => {
          
          return contratos;
        })
      )
  }
}
