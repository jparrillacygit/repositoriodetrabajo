import { TestBed } from '@angular/core/testing';

import { GetTiposIncidenciasService } from './get-tipos-incidencias.service';

describe('GetTiposIncidenciasService', () => {
  let service: GetTiposIncidenciasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetTiposIncidenciasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
