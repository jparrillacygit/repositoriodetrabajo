import { TestBed } from '@angular/core/testing';

import { GetEstadosIncidenciasService } from './get-estados-incidencias.service';

describe('GetEstadosIncidenciasService', () => {
  let service: GetEstadosIncidenciasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetEstadosIncidenciasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
