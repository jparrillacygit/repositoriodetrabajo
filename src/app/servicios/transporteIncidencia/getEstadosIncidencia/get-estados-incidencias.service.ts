import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';
import {HttpParams} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class GetEstadosIncidenciasService {
  constructor(private http: HttpClient,private env: EnvServiciosService) { }
  getTipoIncidencia(id:number,tipo:string){
    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    let title:any={ headers: headers };
    if(tipo!=null){
      const params = new HttpParams().set('tipo',tipo.toString())
        
      title={ headers: headers , params: params}
    }
    return this.http.get<any>(this.env.API_URL + '/transporteIncidencias/estados/'+id, title)
      .pipe(
        tap(contratos => {
          
          return contratos;
        })
      )
  }
  
}
