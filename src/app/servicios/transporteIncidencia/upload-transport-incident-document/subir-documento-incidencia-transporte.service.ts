import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubirDocumentoIncidenciaTransporteService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }

  subirDocumentoIncidenciaTransporte(id:number,form,page? : number, pageSize? : number){

    const formData = new FormData();
    formData.append('file',form.value);

    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });

    return this.http.post<any>(this.env.API_URL + '/transporteIncidencias/upload', { headers: headers, formData: formData})
    .pipe(
      tap(documentoSubido => {
        
        return documentoSubido;
        
      })
    )

  }
}
