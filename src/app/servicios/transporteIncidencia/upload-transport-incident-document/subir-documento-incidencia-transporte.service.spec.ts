import { TestBed } from '@angular/core/testing';

import { SubirDocumentoIncidenciaTransporteService } from './subir-documento-incidencia-transporte.service';

describe('SubirDocumentoIncidenciaTransporteService', () => {
  let service: SubirDocumentoIncidenciaTransporteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubirDocumentoIncidenciaTransporteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
