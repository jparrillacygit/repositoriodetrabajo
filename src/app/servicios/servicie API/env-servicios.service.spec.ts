import { TestBed } from '@angular/core/testing';

import { EnvServiciosService } from './env-servicios.service';

describe('EnvServiciosService', () => {
  let service: EnvServiciosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnvServiciosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
