import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvServiciosService {
  API_URL = 'https://wsaitanaangular.aitanasolutions.com';
  private keyToken:String;
  getkeyToken(){
    if(this.keyToken!=null){
      return this.keyToken;
    }
  }
  setkeyToken(tokenAdd:String){
    this.keyToken=tokenAdd;
  }
  constructor() { }
}
