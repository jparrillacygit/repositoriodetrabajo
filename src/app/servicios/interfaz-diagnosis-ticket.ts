export interface DiagnosisTickets{
   pin?: string;
   fecha?: string;
   problema?: number;
   cantidad?: string;
   almacenSap?: string;
   descripcion?: string;
   
}