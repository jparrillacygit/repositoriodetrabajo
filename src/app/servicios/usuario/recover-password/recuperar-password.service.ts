import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';

@Injectable({
  providedIn: 'root'
})
export class RecuperarPasswordService {
  // 03/05/2021 No esta implementado.
  constructor(private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvServiciosService,) { }
  pedirCorreo(email:String){
    const headers={
      'Authentication': 'Basic MzUzYjMwMmM0NDU3NGY1NjUwNDU2ODdlNTM0ZTdkNmE6Mjg2OTI0Njk3ZTYxNWE2NzJhNjQ2YTQ5MzU0NTY0NmM='
    };
    return this.http.post<any>(this.env.API_URL + '/auth/recuperarPassword',
      {email: email },{headers}
    ).pipe(
      tap(token => {
        
        this.storage.setItem('token', token)
        .then(
          () => {
            console.log('Token Stored');
          },
          error => console.error('Error storing item', error)
        );
        
        return token;
      }),
    );
  }

  cambiarPass(security_code:string,password:String,email:String){
    const headers={
      'Authentication': 'Basic MzUzYjMwMmM0NDU3NGY1NjUwNDU2ODdlNTM0ZTdkNmE6Mjg2OTI0Njk3ZTYxNWE2NzJhNjQ2YTQ5MzU0NTY0NmM='
    };
    return this.http.post<any>(this.env.API_URL + '/auth/changePassword',
      {security_code:security_code,password:password,email: email },{headers}
    ).pipe(
      tap(token => {
        
        this.storage.setItem('token', token)
        .then(
          () => {
            console.log('Token Stored');
          },
          error => console.error('Error storing item', error)
        );
        return token;
      }),
    );
  }

}
