import { TestBed } from '@angular/core/testing';

import { ObtenerDatosUsuarioService } from './obtener-datos-usuario.service';

describe('ObtenerDatosUsuarioService', () => {
  let service: ObtenerDatosUsuarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerDatosUsuarioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
