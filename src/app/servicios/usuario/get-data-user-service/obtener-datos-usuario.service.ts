import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';
import {InterfazUsuario} from '../../interfaz-usuario';

@Injectable({
  providedIn: 'root'
})
export class ObtenerDatosUsuarioService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }

  getUsuario(){
    
    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    return this.http.get<any>(this.env.API_URL + '/usuarios/me', { headers: headers })
    .pipe(
      tap(user => {
        return user;
      })
    )
  }
  getUsuarioConToken(token:string){
    
    this.env.setkeyToken(token);
    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+token
    });
    return this.http.get<any>(this.env.API_URL + '/usuarios/me', { headers: headers })
    .pipe(
      tap(user => {
        return user;
      })
    )
  }

}
    