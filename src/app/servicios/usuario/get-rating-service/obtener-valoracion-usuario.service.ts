import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ObtenerValoracionUsuarioService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService,) { }

  getValoracionUsuario(){
    console.log("TOKEN: "+this.env.getkeyToken());
    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    return this.http.get<any>(this.env.API_URL + '/usuarios/me/valoracion',{ headers: headers })
    .pipe(
      tap(valoracion => {
        return valoracion;
      })
    )
  }
}
