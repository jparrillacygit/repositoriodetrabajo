import { TestBed } from '@angular/core/testing';

import { ObtenerValoracionUsuarioService } from './obtener-valoracion-usuario.service';

describe('ObtenerValoracionUsuarioService', () => {
  let service: ObtenerValoracionUsuarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerValoracionUsuarioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
