import { TestBed } from '@angular/core/testing';

import { ReenviarCodeService } from './reenviar-code.service';

describe('ReenviarCodeService', () => {
  let service: ReenviarCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReenviarCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
