import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ComprobarCodigoSeguridadService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }

  comprobarCodigo(securityCode : string){
     const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    return this.http.post<string>(this.env.API_URL + '/usuarios/config/securityCode',{security_code: securityCode},{ headers: headers })
    .pipe(
      tap(code => {
        return code;
      })
    )
  }
  }
