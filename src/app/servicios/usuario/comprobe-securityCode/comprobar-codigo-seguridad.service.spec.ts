import { TestBed } from '@angular/core/testing';

import { ComprobarCodigoSeguridadService } from './comprobar-codigo-seguridad.service';

describe('ComprobarCodigoSeguridadService', () => {
  let service: ComprobarCodigoSeguridadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComprobarCodigoSeguridadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
