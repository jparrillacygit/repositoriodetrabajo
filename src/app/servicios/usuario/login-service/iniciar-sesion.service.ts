import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { InterfazLogin } from './interfaz-login';
import {FirebaseApp} from '@angular/fire'
import firebase from 'firebase/app';

import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder } from '@angular/forms';
import { GooglePlus } from '@ionic-native/google-plus/ngx';



@Injectable({
  providedIn: 'root'
})
export class IniciarSesionService {
  isLoggedIn = false;
  token:any;
  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvServiciosService,
    private afAuth:AngularFireAuth,
    private fb:FirebaseApp,
    private googlePlus:GooglePlus
  ) { }
  loginConEmailYToken(email:string,token:string){
    
    const headers={
      'Authentication': 'Basic MzUzYjMwMmM0NDU3NGY1NjUwNDU2ODdlNTM0ZTdkNmE6Mjg2OTI0Njk3ZTYxNWE2NzJhNjQ2YTQ5MzU0NTY0NmM='
    };
    return this.http.post<any>(this.env.API_URL + '/auth/token',
      {email: email, token:token,  grant_type:'email'},{headers}
    ).pipe(
      tap(token => {
        
        this.storage.setItem('token', token)
        .then(
          
          () => {
            console.log('Token Stored',token);
          },
          error => console.error('Error storing item', error),
          
        );
        this.token = token;
        
        this.env.setkeyToken(token['access_token']);
        this.isLoggedIn = true;
        return token;
      }),
    );
  }
  login(usuario: String, password: String) {
    const headers={
      'Authentication': 'Basic MzUzYjMwMmM0NDU3NGY1NjUwNDU2ODdlNTM0ZTdkNmE6Mjg2OTI0Njk3ZTYxNWE2NzJhNjQ2YTQ5MzU0NTY0NmM='
    };
    return this.http.post<any>(this.env.API_URL + '/auth/token',
      {username: usuario, password: password,  grant_type:'password'},{headers}
    ).pipe(
      tap(token => {
        
        this.storage.setItem('token', token)
        .then(
          
          () => {
            console.log('Token Stored',token);
          },
          error => console.error('Error storing item', error),
          
        );
        this.token = token;
        
        this.env.setkeyToken(token['access_token']);
        this.isLoggedIn = true;
        return token;
      }),
    );
  }
  async loginConGoogle(){
    
   // const user = this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    
    
    //return user;
    return this.googlePlus.login({}).then(result =>{
      console.log("RESULTADO LOGIN NUEVO:");
      console.log(result)
    })
    .catch(err =>{
      console.log("ERROR");
      console.log(err)
    });
  }
  getToken(){
    if(this.token!=null){
      return this.token;
    }else{
      return console.error("No está instanciado")
    }
  }
   setToken(token:string){
    this.token=token;
  }
  
}

