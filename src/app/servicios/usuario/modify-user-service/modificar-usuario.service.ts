import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';
import {InterfazUsuario} from '../../interfaz-usuario';

@Injectable({
  providedIn: 'root'
})
export class ModificarUsuarioService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }

  modificarUsuario(userId : number, nombre : string, apellidos:  string){
    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    
    return this.http.put<InterfazUsuario>(this.env.API_URL + '/usuarios/:'+userId,{nombre: nombre, apellidos: apellidos},{ headers: headers })
    .pipe(
      tap(user => {
        return user;
      })
    )
  }
}
