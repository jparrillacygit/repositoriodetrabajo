import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ObtenerDocumentacionContratoService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }

  getDocumentacionContrato(id:number,page? : number, pageSize? : number){

    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });

    const params = new HttpParams().set('page',page.toString()).set('pageSize',pageSize.toString());

    return this.http.get<any>(this.env.API_URL + '/contratos/'+id + "/documentos", { headers: headers, params: params})
      .pipe(
        tap(documentacionContrato => {
          
          return documentacionContrato;
          
        })
      )

  }
}
