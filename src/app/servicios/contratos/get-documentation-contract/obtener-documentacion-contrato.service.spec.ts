import { TestBed } from '@angular/core/testing';

import { ObtenerDocumentacionContratoService } from './obtener-documentacion-contrato.service';

describe('ObtenerDocumentacionContratoService', () => {
  let service: ObtenerDocumentacionContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerDocumentacionContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
