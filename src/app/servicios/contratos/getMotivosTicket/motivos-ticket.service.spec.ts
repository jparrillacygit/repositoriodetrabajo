import { TestBed } from '@angular/core/testing';

import { MotivosTicketService } from './motivos-ticket.service';

describe('MotivosTicketService', () => {
  let service: MotivosTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotivosTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
