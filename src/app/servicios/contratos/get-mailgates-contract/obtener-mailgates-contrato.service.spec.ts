import { TestBed } from '@angular/core/testing';

import { ObtenerMailgatesContratoService } from './obtener-mailgates-contrato.service';

describe('ObtenerMailgatesContratoService', () => {
  let service: ObtenerMailgatesContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerMailgatesContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
