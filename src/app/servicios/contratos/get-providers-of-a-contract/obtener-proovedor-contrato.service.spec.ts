import { TestBed } from '@angular/core/testing';

import { ObtenerProovedorContratoService } from './obtener-proovedor-contrato.service';

describe('ObtenerProovedorContratoService', () => {
  let service: ObtenerProovedorContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerProovedorContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
