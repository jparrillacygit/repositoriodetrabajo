import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';
import {HttpParams} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ParadasContratoService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }
  getParadas(id:number,page? : number, pageSize? : number, parametrosContrato?: number){

    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    let title:any={ headers: headers };
    if(page!=null){
      const params = new HttpParams().set('page',page.toString()).set('pageSize',pageSize.toString()).set('parametrosContrato', parametrosContrato.toString());
      title={ headers: headers , params: params}
    }
      return this.http.get<any>(this.env.API_URL + '/contratos/'+id+'/paradas',title )
      .pipe(
        tap(contratos => {
          
          return contratos;
        })
      )

  }
}
