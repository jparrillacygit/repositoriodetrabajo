import { TestBed } from '@angular/core/testing';

import { ParadasContratoService } from './paradas-contrato.service';

describe('ParadasContratoService', () => {
  let service: ParadasContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParadasContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
