import { TestBed } from '@angular/core/testing';

import { ObtenerContratosService } from './obtener-contratos.service';

describe('ObtenerContratosService', () => {
  let service: ObtenerContratosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerContratosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
