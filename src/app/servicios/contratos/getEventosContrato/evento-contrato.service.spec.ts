import { TestBed } from '@angular/core/testing';

import { EventoContratoService } from './evento-contrato.service';

describe('EventoContratoService', () => {
  let service: EventoContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventoContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
