import { TestBed } from '@angular/core/testing';

import { ObtenerCierresPredefinidosContratoService } from './obtener-cierres-predefinidos-contrato.service';

describe('ObtenerCierresPredefinidosContratoService', () => {
  let service: ObtenerCierresPredefinidosContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerCierresPredefinidosContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
