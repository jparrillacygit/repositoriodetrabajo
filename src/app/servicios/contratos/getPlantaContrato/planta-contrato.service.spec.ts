import { TestBed } from '@angular/core/testing';

import { PlantaContratoService } from './planta-contrato.service';

describe('PlantaContratoService', () => {
  let service: PlantaContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlantaContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
