import { TestBed } from '@angular/core/testing';

import { ObtenerAlegacionesContratoService } from './obtener-alegaciones-contrato.service';

describe('ObtenerAlegacionesContratoService', () => {
  let service: ObtenerAlegacionesContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerAlegacionesContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
