import { TestBed } from '@angular/core/testing';

import { ObtenerPartesContratoService } from './obtener-partes-contrato.service';

describe('ObtenerPartesContratoService', () => {
  let service: ObtenerPartesContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerPartesContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
