import { TestBed } from '@angular/core/testing';

import { ObtenerMotivosCitaContratoService } from './obtener-motivos-cita-contrato.service';

describe('ObtenerMotivosCitaContratoService', () => {
  let service: ObtenerMotivosCitaContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerMotivosCitaContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
