import { TestBed } from '@angular/core/testing';

import { HorariosContratoService } from './horarios-contrato.service';

describe('HorariosContratoService', () => {
  let service: HorariosContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HorariosContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
