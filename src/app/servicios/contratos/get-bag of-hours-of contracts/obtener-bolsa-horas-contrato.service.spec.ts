import { TestBed } from '@angular/core/testing';

import { ObtenerBolsaHorasContratoService } from './obtener-bolsa-horas-contrato.service';

describe('ObtenerBolsaHorasContratoService', () => {
  let service: ObtenerBolsaHorasContratoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerBolsaHorasContratoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
