export interface InterfazUsuario{
    id: number;
    dni?: string,
    username: string,
    nombre: string,
    apellido: string,
    email: string,
    provincia?: string,
    localidad?: string,
    codigo_Postal?: string,
    notificaciones_push: number
    

}