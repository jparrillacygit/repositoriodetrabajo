export interface InterfazTicket{
    titulo?: string,
    id?: number,
    direccion?: string,
    cliente?: string,
    estado?: string,
    tiempo?: string,
    momento? : string,
    telefono?:string,
    movil?:string,
    idProducto?: string,
    marcaProducto?: string,
    modeloProducto?: string,
    descripcionCorta?:string
    descripcionLarga?:string
    
    latitud?: string;
    longitud?: string
    

}