import { TestBed } from '@angular/core/testing';

import { ReportErroresService } from './report-errores.service';

describe('ReportErroresService', () => {
  let service: ReportErroresService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportErroresService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
