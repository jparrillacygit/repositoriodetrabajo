import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { ObtenerTicketService } from '../get-ticket/obtener-ticket.service';

@Injectable({
  providedIn: 'root'
})
export class ObtenerTicketsService {
  numTickets:String[]=[];
  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }

  getTickets(page? : number,fecha?:string ,pageSize? : number, parametrosTicket?: number,){

    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });

    if(page !=null || pageSize != null || parametrosTicket!=null){
      
      let params = new HttpParams().set('page',page+'');
        
      if(fecha !=null){
        console.log('Fecha: '+fecha);
        
        params = new HttpParams().set('page',page+'').set('date', fecha);;
       
      }
      if(pageSize!=null){
        
        params.set('pageSize',pageSize+'');
      }
      if(parametrosTicket!=null){
        params.set('parametrosTicket', parametrosTicket+'')
      }
      console.log('Pagina: '+params.get('page'));
      return this.http.get<any>(this.env.API_URL + '/tickets', { headers: headers , params: params})
      .pipe(
        tap(tickets => {
          
          return tickets;
        })
      )

    }else {
      console.log("NO ME HE METIDO EN LOS PARAMETROS!!");
      //?ID=1206135
      return this.http.get<any>(this.env.API_URL + '/tickets', { headers: headers})
      .pipe(
        tap(tickets => {
          return tickets;
        })
      )
        // Citas ID=1203712
        // Diagnosis ID=1206135
    }


  }
  


}