import { TestBed } from '@angular/core/testing';

import { ObtenerTicketsService } from './obtener-tickets.service';

describe('ObtenerTicketsService', () => {
  let service: ObtenerTicketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerTicketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
