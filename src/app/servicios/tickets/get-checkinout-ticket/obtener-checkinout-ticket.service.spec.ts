import { TestBed } from '@angular/core/testing';

import { ObtenerCheckinoutTicketService } from './obtener-checkinout-ticket.service';

describe('ObtenerCheckinoutTicketService', () => {
  let service: ObtenerCheckinoutTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerCheckinoutTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
