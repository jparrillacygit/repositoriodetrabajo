import { TestBed } from '@angular/core/testing';

import { ObtenerBugsTicketService } from './obtener-bugs-ticket.service';

describe('ObtenerBugsTicketService', () => {
  let service: ObtenerBugsTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerBugsTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
