import { TestBed } from '@angular/core/testing';

import { ObtenerCambiosTicketService } from './obtener-cambios-ticket.service';

describe('ObtenerCambiosTicketService', () => {
  let service: ObtenerCambiosTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerCambiosTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
