import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { tap } from 'rxjs/operators';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';

@Injectable({
  providedIn: 'root'
})
export class NotificacionesService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }
  getFinancierosTicket(id:number,pagina?:number,sizePagina?:number){
    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    if(!pagina==null){
      
      let params= new HttpParams();
      params=params.append('page',pagina+"");
      params=params.append('pageSize',sizePagina+"");
      
      return this.http.get<any>(this.env.API_URL + '/tickets/'+id+'/notificaciones',
      { headers: headers,params:params }
    )
    .pipe(
      tap(data=> {
        console.log(data);
        
      },
      )
    );
    }else{
      
      
      return this.http.get<any>(this.env.API_URL + '/tickets/'+id+'/notificaciones',
      { headers: headers }
      )
      .pipe(
        tap(data=> {
          console.log(data);
          
        },
        )
      );
    }
  }
}
