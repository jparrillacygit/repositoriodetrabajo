import { TestBed } from '@angular/core/testing';

import { ObtenerSeguimientoYCitaService } from './obtener-seguimiento-ycita.service';

describe('ObtenerSeguimientoYCitaService', () => {
  let service: ObtenerSeguimientoYCitaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerSeguimientoYCitaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
