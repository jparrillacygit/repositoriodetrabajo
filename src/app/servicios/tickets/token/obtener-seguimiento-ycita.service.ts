import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { tap } from 'rxjs/operators';
import { ObtenerTicketsService } from '../get-tickets/obtener-tickets.service';
@Injectable({
  providedIn: 'root'
})
export class ObtenerSeguimientoYCitaService {
  
  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService,private tickets:ObtenerTicketsService) { }
  listaRuta=['seguimientos','citas','diagnosis','envios','presupuestos'];
  
  getDetallesTickets(id:number,eleccion:number,pagina?:number,sizePagina?:number,){
    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });
    if(!pagina==null){
      
      let params= new HttpParams().set('page',pagina+"");
     
      
      return this.http.get<any>(this.env.API_URL + '/tickets/'+id+'/'+this.listaRuta[eleccion],
      { headers: headers,params:params }
    )
    .pipe(
      tap(data=> {
        console.log(data);
        
      },
      )
    );
    }else{
      console.log("Ruta:"+this.listaRuta[eleccion]);
      
      return this.http.get<any>(this.env.API_URL + '/tickets/'+id+'/'+this.listaRuta[eleccion],
      { headers: headers }
      )
      .pipe(
        tap(data=> {
          console.log(data);
          
        },
        )
      );
    }
  }


}


