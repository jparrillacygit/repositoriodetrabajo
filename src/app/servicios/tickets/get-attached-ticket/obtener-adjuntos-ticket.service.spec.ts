import { TestBed } from '@angular/core/testing';

import { ObtenerAdjuntosTicketService } from './obtener-adjuntos-ticket.service';

describe('ObtenerAdjuntosTicketService', () => {
  let service: ObtenerAdjuntosTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerAdjuntosTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
