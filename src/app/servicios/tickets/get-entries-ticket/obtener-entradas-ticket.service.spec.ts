import { TestBed } from '@angular/core/testing';

import { ObtenerEntradasTicketService } from './obtener-entradas-ticket.service';

describe('ObtenerEntradasTicketService', () => {
  let service: ObtenerEntradasTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerEntradasTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
