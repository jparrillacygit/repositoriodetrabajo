import { TestBed } from '@angular/core/testing';

import { ObtenerCambiosInternosTicketService } from './obtener-cambios-internos-ticket.service';

describe('ObtenerCambiosInternosTicketService', () => {
  let service: ObtenerCambiosInternosTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerCambiosInternosTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
