import { TestBed } from '@angular/core/testing';

import { ObtenerKpisTicketService } from './obtener-kpis-ticket.service';

describe('ObtenerKpisTicketService', () => {
  let service: ObtenerKpisTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerKpisTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
