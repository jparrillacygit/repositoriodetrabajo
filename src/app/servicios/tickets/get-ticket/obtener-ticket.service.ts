import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../../servicie API/env-servicios.service';
import { HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ObtenerTicketService {

  constructor(private http: HttpClient,private storage: NativeStorage,private env: EnvServiciosService) { }

  getTicket(ticketId: number){

    const headers = new HttpHeaders({
      'Authentication': 'Bearer '+this.env.getkeyToken()
    });

    return this.http.get<any>(this.env.API_URL + '/tickets/' + ticketId, { headers: headers})
    .pipe(
      tap(ticket => {
        return ticket;
      })
    )

  }
}
