import { TestBed } from '@angular/core/testing';

import { ObtenerTicketService } from './obtener-ticket.service';

describe('ObtenerTicketService', () => {
  let service: ObtenerTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObtenerTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
