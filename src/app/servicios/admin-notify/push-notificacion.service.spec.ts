import { TestBed } from '@angular/core/testing';

import { PushNotificacionService } from './push-notificacion.service';

describe('PushNotificacionService', () => {
  let service: PushNotificacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PushNotificacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
