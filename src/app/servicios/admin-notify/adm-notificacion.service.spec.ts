import { TestBed } from '@angular/core/testing';

import { AdmNotificacionService } from './adm-notificacion.service';

describe('AdmNotificacionService', () => {
  let service: AdmNotificacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdmNotificacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
