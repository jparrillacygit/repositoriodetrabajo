import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvServiciosService } from '../servicie API/env-servicios.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdmNotificacionService {
  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvServiciosService,
  ) { }
  habilitarNotificaciones(valor:number){
    const headers={
      'Authentication': 'Bearer '+this.env.getkeyToken()
    };
    return this.http.put<any>(this.env.API_URL + '/usuarios/config/modificarNotificacionesPush',
      {valor: valor },{headers}
    ).pipe(
      tap(token => {
        
        this.storage.setItem('token', token)
        .then(
          
          () => {
            console.log('Token Stored',token);
          },
          error => console.error('Error storing item', error),
          
        );
        return token;
      }),
    );
  }
  registrarNotificacion(tokenDevice:string){
    const headers={
      'Authentication': 'Bearer '+this.env.getkeyToken()
    };
    return this.http.post<any>(this.env.API_URL + '/usuarios/config/registerTokenDevice',
      {tokenDevice: tokenDevice },{headers}
    ).pipe(
      tap(token => {
        
        this.storage.setItem('token', token)
        .then(
          
          () => {
            console.log('Token Stored',token);
          },
          error => console.error('Error storing item', error),
          
        );
        return token;
      }),
    );
  }
  desRegistrarToken(tokenDevice:string){
    const headers={
      'Authentication': 'Bearer '+this.env.getkeyToken
    };
    return this.http.put<any>(this.env.API_URL + '/usuarios/config/unregisterTokenDevice',
      {tokenDevice: tokenDevice },{headers}
    ).pipe(
      tap(token => {
        
        this.storage.setItem('token', token)
        .then(
          
          () => {
            console.log('Token Stored',token);
          },
          error => console.error('Error storing item', error),
          
        );
        return token;
      }),
    );
  }
}
