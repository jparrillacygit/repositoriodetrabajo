import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ComponentesModule } from './componentes/componentes.module';
import { HttpClientModule } from '@angular/common/http';
import {ProovedorService} from '../app/provider/proovedor.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import {AngularFireAuth, AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFireModule} from '@angular/fire';
import { environment } from 'src/environments/environment';
import{GooglePlus} from '@ionic-native/google-plus/ngx'


//GELOCALIZACION
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';

//Social Sharing plugin 
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  
  imports: [BrowserModule, HttpClientModule,IonicModule.forRoot(), AppRoutingModule, ComponentesModule,
  AngularFireModule.initializeApp(environment.firebaseConfig),
  AngularFireAuthModule
],
 
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ProovedorService,NativeStorage,GooglePlus,
    Geolocation,
    NativeGeocoder,
    SocialSharing],
  bootstrap: [AppComponent],
})
export class AppModule {}
