import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Aitana005bComponent } from './menu/aitana005b.component';
import { IonicModule } from '@ionic/angular';
import { BarraUsuarioComponent } from './barra-usuario/barra-usuario.component';
import { FooterComponent } from '../../app/componentes/footer/footer.component';
import { HeaderSecundarioAitana004Component } from './header-secundario-aitana004/header-secundario-aitana004.component';
import { HeaderSecundarioTicketsComponent } from './header-secundario-tickets/header-secundario-tickets.component';
import { HeaderSecundarioAitana006Component } from './header-secundario-aitana006/header-secundario-aitana006.component';



@NgModule({
  declarations: [Aitana005bComponent,BarraUsuarioComponent,FooterComponent,
    HeaderSecundarioAitana004Component,HeaderSecundarioTicketsComponent,HeaderSecundarioAitana006Component],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports:[Aitana005bComponent,BarraUsuarioComponent,FooterComponent,
    HeaderSecundarioAitana004Component,HeaderSecundarioTicketsComponent,HeaderSecundarioAitana006Component]
})
export class ComponentesModule { }
