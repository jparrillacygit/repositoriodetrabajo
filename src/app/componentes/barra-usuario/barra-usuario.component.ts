import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, MenuController } from '@ionic/angular';
import { ComunicadorService } from 'src/app/servicios/comunicador.service';
import {ObtenerDatosUsuarioService} from '../../servicios/usuario/get-data-user-service/obtener-datos-usuario.service';
import { InterfazUsuario} from '../../servicios/interfaz-usuario';
import { Aitana005bComponent } from '../menu/aitana005b.component';

@Component({
  selector: 'app-barra-usuario',
  templateUrl: './barra-usuario.component.html',
  styleUrls: ['./barra-usuario.component.scss'],
})
export class BarraUsuarioComponent implements OnInit {

  private usuario: InterfazUsuario;
  
  constructor(private menuCtrl:MenuController,private obtenerDatosUsuarioService: ObtenerDatosUsuarioService,
    private comunicador:ComunicadorService,
    private alertas:AlertController,
    private router:Router,
    public loadingController: LoadingController
    ) { }

  ngOnInit() {
    this.alertaDeCarga();
    
    this.obtenerDatosUsuarioService.getUsuario().subscribe(
      (data) => {

        console.log(data);

        this.usuario = {
      
          id: data.id,
          username:  data.username,
          nombre:  data.nombre,
          apellido:  data.apellidos,
          email:  data.email,
          notificaciones_push:  data.notificaciones_push
        };
        console.log("HOLA ENTRE");
        this.menuCtrl.enable(true);
      },
      error => {
        
        this.router.navigate(['/aitana001a']);
        console.log(error);
        
        
         
        
      }
    );

    this.usuario = {
      
      id: 0,
      username:  '',
      nombre:  '',
      apellido:  '',
      email:  '',
      notificaciones_push: 0
    };
  }
  
  abrirMenu(){
    
    this.menuCtrl.toggle();
    this.comunicador.getMenu().obtenerUsuario();
  }
  async alertaDeCarga() {
    const loading = await this.loadingController.create({
      message: 'Cargando los datos espere un momento...',
      duration: 1000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
}
