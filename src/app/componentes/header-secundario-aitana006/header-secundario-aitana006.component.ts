import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-secundario-aitana006',
  templateUrl: './header-secundario-aitana006.component.html',
  styleUrls: ['./header-secundario-aitana006.component.scss'],
})
export class HeaderSecundarioAitana006Component implements OnInit {
  @Input() titulo:string="";
  @Input() ticket:number=105381;
  @Input() opcionVuelta:number=1;
  eleccion:string[]=['/aitana005a','/aitana006a','/aitana006e','/aitana006h','/aitana006l','/aitana006p']

  constructor(private router: Router) { }

  ngOnInit() {}
  volver(){
    console.log("TICKET: "+this.ticket)
    if(this.opcionVuelta==0){
      this.router.navigate([this.eleccion[this.opcionVuelta]]);
    }else{
      this.router.navigate([this.eleccion[this.opcionVuelta],this.ticket]);
    }
    
  }
}
