import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HeaderSecundarioAitana006Component } from './header-secundario-aitana006.component';

describe('HeaderSecundarioAitana006Component', () => {
  let component: HeaderSecundarioAitana006Component;
  let fixture: ComponentFixture<HeaderSecundarioAitana006Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderSecundarioAitana006Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderSecundarioAitana006Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
