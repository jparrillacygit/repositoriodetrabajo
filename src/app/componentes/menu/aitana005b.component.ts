import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonMenu, MenuController } from '@ionic/angular';
import { ComunicadorService } from 'src/app/servicios/comunicador.service';
import {ObtenerDatosUsuarioService} from '../../servicios/usuario/get-data-user-service/obtener-datos-usuario.service';
import { InterfazUsuario} from '../../servicios/interfaz-usuario';

@Component({
  selector: 'app-aitana005b',
  templateUrl: './aitana005b.component.html',
  styleUrls: ['./aitana005b.component.scss'],
})
export class Aitana005bComponent implements OnInit {
  
  usuario : InterfazUsuario;

  constructor(private menuCtrl:MenuController,
    private router:Router, 
    private obtenerDatosUsuarioService: ObtenerDatosUsuarioService, 
    private comunicador:ComunicadorService,
    
    ) {
    this.menuCtrl.enable(false);
   }

  ngOnInit() {
    console.log("HOLA ESTOY AQUI");
    
    this.obtenerUsuario();
    this.comunicador.setAitana005bComponent(this);
  }
  

  obtenerUsuario( ) {
    this.obtenerDatosUsuarioService.getUsuario().subscribe(
      (data) => {
  
        console.log(data);
  
        this.usuario = {
      
          id: data.id,
          username:  data.username,
          nombre:  data.nombre,
          apellido:  data.apellidos,
          email:  data.email,
          notificaciones_push:  data.notificaciones_push
        };
        console.log("USUARIO: "+this.usuario);
        
        
      },
      error => {
        console.log("ERROR: "+error);
        
      }
    );
  
    this.usuario = {
      
      id: 0,
      username:  '',
      nombre:  '',
      apellido:  '',
      email:  '',
      notificaciones_push: 0
    };
  }

  quitarMenu(){
    this.menuCtrl.close();
  }
  entrarOpciones(opcion:number){
    switch(opcion){
      case 1: this.router.navigate(['/aitana004a']);break;
      case 2: this.router.navigate(['/aitana005a']);break;
      case 3: this.router.navigate(['/aitana001a']);break;
    }
    this.menuCtrl.close();
  }
  
}
