import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HeaderSecundarioAitana004Component } from './header-secundario-aitana004.component';

describe('HeaderSecundarioAitana004Component', () => {
  let component: HeaderSecundarioAitana004Component;
  let fixture: ComponentFixture<HeaderSecundarioAitana004Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderSecundarioAitana004Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderSecundarioAitana004Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
