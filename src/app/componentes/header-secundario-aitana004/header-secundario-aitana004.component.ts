import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-secundario-aitana004',
  templateUrl: './header-secundario-aitana004.component.html',
  styleUrls: ['./header-secundario-aitana004.component.scss'],
})
export class HeaderSecundarioAitana004Component implements OnInit {
  @Input() titulo:string="";
  @Input() numIcono:number=0;
  @Input() ruta:string="/aitana004a";
  textIcono:string="";
  constructor(private router:Router) { }

  ngOnInit() {
   
    switch(this.numIcono){
      case 1:this.textIcono="arrow-back";break;
      case 2: this.textIcono="person-outline";break;
      case 3: this.textIcono="wallet-outline";break;
    }
    console.log("RUTA:"+this.ruta);
  }
  volver(){
    this.router.navigate([this.ruta]);
  }
}
